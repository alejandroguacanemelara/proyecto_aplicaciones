<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Prod_venDAO.php";

  class Prod_ven{

    private $id_ventafk;
    private $id_productofk;
    private $cantidad;
    private $conexion;
    private $prod_venDAO;

        public function getId_ventafk(){
            return $this -> id_ventafk;
        }

        public function getId_productofk(){
            return $this -> id_productofk;
        }

        public function getCantidad(){
            return $this -> cantidad;
        }

        public function Prod_ven($id_ventafk="",$id_productofk="",$cantidad=""){
              $this -> id_ventafk = $id_ventafk;
              $this -> id_productofk = $id_productofk;
              $this -> cantidad = $cantidad;
              $this -> conexion = new Conexion();
              $this -> prod_venDAO = new Prod_venDAO($this -> id_ventafk, $this -> id_productofk, $this -> cantidad);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> prod_venDAO -> insertar());
          //echo $this -> prod_venDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function consultarVentas_v(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> prod_venDAO -> consultarVentas_v());
          $pro_venarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prod_vens = new Prod_ven("",$resultado[0],$resultado[1]);
            array_push($pro_venarray,$prod_vens);
          }
          $this -> conexion -> cerrar();
          return $pro_venarray;
        }

  }
?>
