<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ClienteDAO.php";

  class Cliente{

    private $idCliente;
    private $nombre;
    private $apellido;
    private $contraseña;
    private $correo;
    private $foto;
    private $sexo;
    private $estado;
    private $direccion;
    private $conexion;
    private $clienteDAO;

        public function getIdCliente(){
            return $this -> idCliente;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getApellido(){
            return $this -> apellido;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getContraseña(){
            return $this -> contraseña;
        }
        public function getFoto(){
            return $this -> foto;
        }

        public function getSexo(){
            return $this -> sexo;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getDireccion(){
            return $this -> direccion;
        }

        public function Cliente($idCliente="",$nombre="",$apellido="",$contraseña="",$correo="",$foto="", $sexo="", $estado="",$direccion=""){
              $this -> idCliente = $idCliente;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> contraseña = $contraseña;
              $this -> correo = $correo;
              $this -> foto = $foto;
              $this -> sexo = $sexo;
              $this -> estado = $estado;
              $this -> direccion = $direccion;
              $this -> conexion = new Conexion();
              $this -> clienteDAO = new ClienteDAO($this -> idCliente, $this -> nombre, $this -> apellido, $this -> contraseña, $this -> correo, $this -> foto, $this -> sexo, $this -> estado, $this -> direccion);
        }

        public function consultarid(){
                $this -> conexion -> abrir();
                $this -> conexion -> ejecutar($this -> clienteDAO -> consultarid());
                $this -> conexion -> cerrar();
                $resultado = $this -> conexion -> extraer();
                $this -> idCliente = $resultado[0];
        }

        public function actualizar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> clienteDAO -> actualizar());
            //echo $this -> clienteDAO -> actualizar();
            $this -> conexion -> cerrar();
        }

        public function consultar(){
                $this -> conexion -> abrir();
                $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
                $this -> conexion -> cerrar();
                $resultado = $this -> conexion -> extraer();
                $this -> nombre = $resultado[0];
                $this -> apellido = $resultado[1];
                $this -> correo = $resultado[2];
                $this -> foto = $resultado[3];
                $this -> sexo = $resultado[4];
                $this -> direccion = $resultado[5];
                $this -> estado = $resultado[6];
        }

        public function consultarClientes(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> clienteDAO -> consultarClientes());
          $cliarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $cli = new Cliente($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[6], $resultado[4], $resultado[5]);
            array_push($cliarray,$cli);
          }
          $this -> conexion -> cerrar();
          return $cliarray;
        }

        public function consultarCantidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> clienteDAO -> consultarCantidad());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function validar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> clienteDAO -> validar());
            $this -> conexion -> cerrar();
            if ($this -> conexion -> numFilas() == 1){
                $resultado = $this -> conexion -> extraer();
                $this -> idCliente = $resultado[0];
                $this -> nombre = $resultado[1];
                $this -> apellido = $resultado[2];
                $this -> estado = $resultado[3];
                return true;
            }else {
                return false;
            }
        }

        public function existeCorreo(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> clienteDAO -> existeCorreo());
            $this -> conexion -> cerrar();
            return $this -> conexion -> numFilas();
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $codigoActivacion = rand(1000,9999);
          $this -> conexion -> ejecutar($this -> clienteDAO -> insertar($codigoActivacion));
          $this -> conexion -> cerrar();
          $url = "http://localhost/proyecto_aplicaciones/index.php?pid=" .
                      base64_encode("vista/cliente/activarcuenta.php") . "&correo=" .
                      $this -> correo . "&codigoActivacion=" .
                      base64_encode($codigoActivacion);
          return $url;
          /*$asunto = "[Tienda Virtual] Activación de Nuevo Usuario";
          $mensaje = "Active su cuenta con la URL " . $url;
          $encabezado = array(
              "From" => "contacto@itiud.org",
              "Cc" => "haflorezf@udistrital.edu.co",
              "Bcc" => "hectorarturo@yahoo.com"
          );
          mail($this -> correo, $asunto, $mensaje, $encabezado);*/
      }

      public function activarCliente($codigoActivacion){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> clienteDAO -> verificarCodigoActivacion($codigoActivacion));
          if ($this -> conexion -> numFilas() == 1){
              $this -> conexion -> ejecutar($this -> clienteDAO -> activar());
              $this -> conexion -> cerrar();
              return true;
          }else {
              $this -> conexion -> cerrar();
              return false;
          }
      }

      public function actualizar_Estado_Cli(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> clienteDAO -> actualizar_Estado_Cli());
          //echo $this -> clienteDAO -> actualizar();
          $this -> conexion -> cerrar();
      }

      public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarPaginacion($cantidad, $pagina));
        $cliarray = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
          $cli = new Cliente($resultado[0], $resultado[1], $resultado[2],"", $resultado[3],$resultado[6], $resultado[4], $resultado[5]);
          array_push($cliarray,$cli);
        }
        $this -> conexion -> cerrar();
        return $cliarray;
      }

  }
?>
