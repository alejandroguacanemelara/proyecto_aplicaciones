<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/AdministradorDAO.php";

  class Administrador{

    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $contraseña;
    private $correo;
    private $foto;
    private $conexion;
    private $administradorDAO;

        public function getIdAdministrador(){
            return $this -> idAdministrador;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getApellido(){
            return $this -> apellido;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getContraseña(){
            return $this -> contraseña;
        }
        public function getFoto(){
            return $this -> foto;
        }

        public function Administrador($idAdministrador="",$nombre="",$apellido="",$contraseña="",$correo="",$foto=""){
              $this -> idAdministrador = $idAdministrador;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> contraseña = $contraseña;
              $this -> correo = $correo;
              $this -> foto = $foto;
              $this -> conexion = new Conexion();
              $this -> administradorDAO = new AdministradorDAO(  $this -> idAdministrador,$this -> nombre,  $this -> apellido, $this -> contraseña,  $this -> correo, $this -> foto);
        }

        public function consultarAdministradores(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> consultarAdministradores());
          $arrayAdmin = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $admin = new Administrador($resultado[0], $resultado[1], $resultado[2],"", $resultado[3], $resultado[4]);
            array_push($arrayAdmin,$admin);
          }
          $this -> conexion -> cerrar();
          return $arrayAdmin;
        }

        public function validar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> administradorDAO -> validar());
            $this -> conexion -> cerrar();

            if($this -> conexion -> numFilas() == 1){
                $resultado = $this -> conexion -> extraer();
                $this -> idAdministrador = $resultado[0];
                $this -> nombre = $resultado[1];
                $this -> apellido = $resultado[2];
                return true;
            }else {
              return false;
            }
        }

        public function consultar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
          $this -> conexion -> cerrar();

          if($this -> conexion -> numFilas() == 1){
              $resultado = $this -> conexion -> extraer();
              $this -> nombre = $resultado[0];
              $this -> apellido = $resultado[1];
              $this -> correo = $resultado[2];
              $this -> foto = $resultado[3];
              return true;
          }else {
            return false;
          }
        }

        public function actualizar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> administradorDAO -> actualizar());
            //echo $this -> administradorDAO -> actualizar();
            $this -> conexion -> cerrar();
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> administradorDAO -> insertar());
          //echo $this -> administradorDAO -> insertar();
          $this -> conexion -> cerrar();
        }
  }

?>
