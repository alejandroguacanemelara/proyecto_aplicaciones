<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProveedorDAO.php";

  class Proveedor{

    private $idProveedor;
    private $nombre;
    private $direccion;
    private $contraseña;
    private $correo;
    private $telefono;
    private $foto;
    private $estado;
    private $conexion;
    private $proveedorDAO;

        public function getIdProveedor(){
            return $this -> idProveedor;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getDireccion(){
            return $this -> direccion;
        }

        public function getCorreo(){
            return $this -> correo;
        }

        public function getContraseña(){
            return $this -> contraseña;
        }

        public function getTelefono(){
            return $this -> telefono;
        }

        public function getEstado(){
            return $this -> estado;
        }

        public function getFoto(){
            return $this -> foto;
        }

        public function Proveedor($idProveedor="",$nombre="",$direccion="",$contraseña="",$correo="",$telefono="",$estado="",$foto=""){
              $this -> idProveedor = $idProveedor;
              $this -> nombre = $nombre;
              $this -> direccion = $direccion;
              $this -> contraseña = $contraseña;
              $this -> correo = $correo;
              $this -> telefono = $telefono;
              $this -> estado = $estado;
              $this -> foto = $foto;
              $this -> conexion = new Conexion();
              $this -> proveedorDAO = new ProveedorDAO(  $this -> idProveedor,$this -> nombre,  $this -> direccion, $this -> contraseña,  $this -> correo, $this -> telefono, $this -> estado, $this -> foto);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> proveedorDAO -> insertar());
          //echo $this -> proveedorDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function validar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedorDAO -> validar());
            $this -> conexion -> cerrar();

            if($this -> conexion -> numFilas() == 1){
                $resultado = $this -> conexion -> extraer();
                $this -> idProveedor = $resultado[0];
                $this -> nombre = $resultado[1];
                $this -> estado = $resultado[2];
                return true;
            }else {
              return false;
            }
        }

        public function consultar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> proveedorDAO -> consultar());
          $this -> conexion -> cerrar();

          if($this -> conexion -> numFilas() == 1){
              $resultado = $this -> conexion -> extraer();
              $this -> nombre = $resultado[0];
              $this -> direccion = $resultado[1];
              $this -> correo = $resultado[2];
              $this -> telefono = $resultado[3];
              $this -> foto = $resultado[4];
              $this -> estado = $resultado[5];
              return true;
          }else {
            return false;
          }
        }

        public function consultarCantidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarCantidad());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function consultarProveedores(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarProveedores());
          $provearray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prove = new Proveedor($resultado[0], $resultado[1], $resultado[2],"", $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($provearray,$prove);
          }
          $this -> conexion -> cerrar();
          return $provearray;
        }

        public function consultarPaginacion($cantidad, $pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> proveedorDAO -> consultarPaginacion($cantidad, $pagina));
          $provearray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $prove = new Proveedor($resultado[0], $resultado[1], $resultado[2],"", $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
            array_push($provearray,$prove);
          }
          $this -> conexion -> cerrar();
          return $provearray;
        }

        public function actualizar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedorDAO -> actualizar());
            //echo $this -> proveedorDAO -> actualizar();
            $this -> conexion -> cerrar();
        }

        public function actualizar_Estado_Prov(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedorDAO -> actualizar_Estado_Prov());
            //echo $this -> proveedorDAO -> actualizar();
            $this -> conexion -> cerrar();
        }

    }
?>
