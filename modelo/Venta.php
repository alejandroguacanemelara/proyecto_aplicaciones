<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/VentaDAO.php";

  class Venta{

    private $id_venta;
    private $id_clientefk;
    private $precio_ven;
    private $fecha_ven;
    private $hora_ven;
    private $conexion;
    private $ventaDAO;

        public function getId_venta(){
            return $this -> id_venta;
        }

        public function getId_clientefk(){
            return $this -> id_clientefk;
        }

        public function getPrecio_ven(){
            return $this -> precio_ven;
        }

        public function getFecha_ven(){
            return $this -> fecha_ven;
        }

        public function getHora_ven(){
            return $this -> hora_ven;
        }


        public function Venta($id_venta="",$id_clientefk="",$precio_ven="",$fecha_ven="",$hora_ven=""){
              $this -> id_venta = $id_venta;
              $this -> id_clientefk = $id_clientefk;
              $this -> precio_ven = $precio_ven;
              $this -> fecha_ven = $fecha_ven;
              $this -> hora_ven = $hora_ven;
              $this -> conexion = new Conexion();
              $this -> ventaDAO = new VentaDAO($this -> id_venta, $this -> id_clientefk, $this -> precio_ven, $this -> fecha_ven, $this -> hora_ven);
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> ventaDAO -> insertar());
          //echo $this -> ventaDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function consultarVentas(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> ventaDAO -> consultarVentas());
          $venarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $ven = new Venta($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4]);
            array_push($venarray,$ven);
          }
          $this -> conexion -> cerrar();
          return $venarray;
        }

        public function consultarVentasCli(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> ventaDAO -> consultarVentasCli());
          $venarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $ven = new Venta($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4]);
            array_push($venarray,$ven);
          }
          $this -> conexion -> cerrar();
          return $venarray;
        }

        public function consultarCantidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> ventaDAO -> consultarCantidad());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function consultarPaginacion($cantidad, $pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> ventaDAO -> consultarPaginacion($cantidad, $pagina));
          $venarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $ven = new Venta($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4]);
            array_push($venarray,$ven);
          }
          $this -> conexion -> cerrar();
          return $venarray;
        }

        public function consultarVentasCliPaginacion($cantidad, $pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> ventaDAO -> consultarVentasCliPaginacion($cantidad, $pagina));
          $venarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $ven = new Venta($resultado[0], $resultado[1], $resultado[2],$resultado[3],$resultado[4]);
            array_push($venarray,$ven);
          }
          $this -> conexion -> cerrar();
          return $venarray;
        }

        public function consultarCantidadVenCli(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> ventaDAO -> consultarCantidadVenCli());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }
  }
?>
