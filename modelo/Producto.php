<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";

  class Producto{

    private $idProducto;
    private $nombre;
    private $precio;
    private $foto;
    private $fech_reg;
    private $descripcion;
    private $tipo;
    private $inventario;
    private $conexion;
    private $productoDAO;

        public function getIdProducto(){
            return $this -> idProducto;
        }

        public function getNombre(){
            return $this -> nombre;
        }

        public function getPrecio(){
            return $this -> precio;
        }

        public function getFoto(){
            return $this -> foto;
        }

        public function getFech_reg(){
            return $this -> fech_reg;
        }

        public function getDescripcion(){
            return $this -> descripcion;
        }

        public function getTipo(){
            return $this -> tipo;
        }

        public function getInventario(){
            return $this -> inventario;
        }

        public function Producto($idProducto="",$nombre="",$precio="",$foto="",$fech_reg="",$descripcion="",$tipo="",$inventario=""){
              $this -> idProducto = $idProducto;
              $this -> nombre = $nombre;
              $this -> precio = $precio;
              $this -> foto = $foto;
              $this -> fech_reg = $fech_reg;
              $this -> descripcion = $descripcion;
              $this -> tipo = $tipo;
              $this -> inventario = $inventario;
              $this -> conexion = new Conexion();
              $this -> productoDAO = new ProductoDAO(  $this -> idProducto,$this -> nombre, $this -> precio, $this -> foto, $this -> fech_reg, $this -> descripcion,$this -> tipo, $this -> inventario);
        }

        public function consultarPaginacion($cantidad, $pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> consultarPaginacion($cantidad, $pagina));
          $productos = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
              $pro = new Producto($resultado[0], $resultado[1], $resultado[2],$resultado[5], $resultado[3], $resultado[4],$resultado[6],$resultado[7]);
              array_push($productos, $pro);
          }
          $this -> conexion -> cerrar();
          return $productos;
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> insertar());
          //$this -> productoDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function consultarCantidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> consultarCantidad());
            $this -> conexion -> cerrar();
            return $this -> conexion -> extraer()[0];
        }

        public function consultarProducto(){
              $this -> conexion -> abrir();
              $this -> conexion -> ejecutar($this -> productoDAO -> consultarProducto());
              $this -> conexion -> cerrar();
              $resultado = $this -> conexion -> extraer();
              $this -> nombre = $resultado[0];
              $this -> precio = $resultado[1];
              $this -> descripcion = $resultado[2];
              $this -> foto = $resultado[3];
              $this -> inventario = $resultado[4];
              $this -> tipo = $resultado[5];
        }

        public function consultarProductos(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductos());
          $proarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $pro = new Producto($resultado[0], $resultado[1], $resultado[2],$resultado[5], $resultado[3], $resultado[4],$resultado[6],$resultado[7]);
            array_push($proarray,$pro);
          }
          $this -> conexion -> cerrar();
          return $proarray;
        }

        public function actualizar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> actualizar());
            //echo $this -> productoDAO -> actualizar();
            $this -> conexion -> cerrar();
        }

        public function actualizarInventario(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> productoDAO -> actualizarInventario());
            //echo $this -> productoDAO -> actualizarInventario();
            $this -> conexion -> cerrar();
        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> productoDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $proarray = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
            $pro = new Producto($resultado[0], $resultado[1], $resultado[2],$resultado[5], $resultado[3], $resultado[4],$resultado[6],$resultado[7]);
            array_push($proarray,$pro);
          }
          $this -> conexion -> cerrar();
          return $proarray;
        }

        public function consultarCantidad_filtro($filtro){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this-> productoDAO -> consultarCantidad_filtro($filtro));
            return $this -> conexion -> extraer();
        }

  }

?>
