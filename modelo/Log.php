<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogDAO.php";

  class Log{

    private $id_log;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;
    private $id_actor;
    private $conexion;
    private $LogDAO;

        public function getId_log(){
            return $this -> id_log;
        }

        public function getAccion(){
            return $this -> accion;
        }

        public function getDatos(){
            return $this -> datos;
        }

        public function getFecha(){
            return $this -> fecha;
        }

        public function getHora(){
            return $this -> hora;
        }

        public function getActor(){
            return $this -> actor;
        }

        public function getId_actor(){
            return $this -> id_actor;
        }

        public function Log($id_log="",$accion="",$datos="",$fecha="",$hora="",$actor="",$id_actor=""){
              $this -> id_log = $id_log;
              $this -> accion = $accion;
              $this -> datos = $datos;
              $this -> fecha = $fecha;
              $this -> hora = $hora;
              $this -> actor = $actor;
              $this -> id_actor = $id_actor;
              $this -> conexion = new Conexion();
              $this -> logDAO = new LogDAO($this -> id_log,$this -> accion,  $this -> datos, $this -> fecha,  $this -> hora, $this -> actor,$this -> id_actor);
        }

        public function consultarFiltroPaginacion($filtro,$cantidad,$pagina){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltroPaginacion($filtro,$cantidad,$pagina));
          $Logs = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
              $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
              array_push($Logs, $log);
          }
          $this -> conexion -> cerrar();
          return $Logs;
        }
        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> insertar());
          //echo $this -> logDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function consultarFiltro($filtro){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> logDAO -> consultarFiltro($filtro));
          $Logs = array();
          while(($resultado = $this -> conexion -> extraer()) != null){
              $log = new Log($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6]);
              array_push($Logs, $log);
          }
          $this -> conexion -> cerrar();
          return $Logs;
        }

        public function consultarCantidad($filtro){
          $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logDAO -> consultarCantidad($filtro));
        return $this -> conexion -> extraer();
    }

  }

?>
