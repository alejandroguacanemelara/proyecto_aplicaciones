<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Proveedor_productoDAO.php";

  class Proveedor_producto{

    private $idProducto_FK;
    private $idProveedor_FK;
    private $cantidad;
    private $conexion;
    private $proveedor_productoDAO;

        public function getIdProducto_FK(){
            return $this -> idProducto_FK;
        }

        public function getIdProveedor_FK(){
            return $this -> idProveedor_FK;
        }

        public function getCantidad(){
            return $this -> cantidad;
        }

        public function Proveedor_producto($idProducto_FK="",$idProveedor_FK="",$cantidad=""){
              $this -> idProducto_FK = $idProducto_FK;
              $this -> idProveedor_FK = $idProveedor_FK;
              $this -> cantidad = $cantidad;
              $this -> conexion = new Conexion();
              $this -> proveedor_productoDAO = new Proveedor_productoDAO($this -> idProducto_FK,$this -> idProveedor_FK, $this -> cantidad);
        }


        public function actualizarCantidad(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedor_productoDAO -> actualizarCantidad());
            //echo $this -> proveedor_productoDAO -> actualizarCantidad();
            $this -> conexion -> cerrar();
        }

        public function verificar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedor_productoDAO -> verificar());
            $this -> conexion -> cerrar();
            if ($this -> conexion -> numFilas() == 1){
                $resultado = $this -> conexion -> extraer();
                $this -> cantidad = $resultado[0];
                return true;
            }else {
                return false;
            }
        }

        public function insertar(){
          $this -> conexion -> abrir();
          $this -> conexion -> ejecutar($this -> proveedor_productoDAO -> insertar());
          //echo $this -> proveedor_productoDAO -> insertar();
          $this -> conexion -> cerrar();
        }

        public function consultarprovee_prod(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> proveedor_productoDAO -> consultarprovee_prod());
            //echo $this -> proveedor_productoDAO -> consultarprovee_prod();
            $proarray = array();
            while(($resultado = $this -> conexion -> extraer()) != null){
              $prov_prod = new Proveedor_producto($resultado[1],"", $resultado[0]);
              array_push($proarray,$prov_prod);
            }
            $this -> conexion -> cerrar();
            return $proarray;
        }
  }

?>
