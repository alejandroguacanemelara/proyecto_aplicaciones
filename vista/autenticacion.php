<?php
  $correo = $_POST["correo"];
  $contra = $_POST["contraseña"];
  $admin = new Administrador("","","",$contra,$correo);
  $provee = new Proveedor("","","",$contra,$correo);
  $cliente = new Cliente ("","","",$contra,$correo);

  if($admin -> validar()){
    $_SESSION["id"] = $admin -> getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    date_default_timezone_set('America/Bogota');
    $fecha = date("Y-m-d");
    $hora = date("H:i:s");

    $log = new Log("","InicioSesionAdministrador", $correo."&".$admin -> getNombre()."&".$admin -> getApellido(),$fecha,$hora,"0",$admin -> getIdAdministrador());
    $log -> insertar();
    header("Location: index.php?pid=".base64_encode("vista/sesionAdmin.php"));
  }else if($provee -> validar()){
    if ($provee -> getEstado() == 1) {
      $_SESSION["id"] = $provee -> getIdProveedor();
      $_SESSION["rol"] = "Proveedor";
      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","InicioSesionProveedor", $correo."&".$provee -> getNombre(),$fecha,$hora,"1",$provee -> getIdProveedor());
      $log -> insertar();
      header("Location: index.php?pid=".base64_encode("vista/sesionProveedor.php"));
    }elseif ($provee -> getEstado() == 0) {
      header("Location: index.php?error=3");
    }
  }elseif ($cliente -> validar()){
    if($cliente -> getEstado() == -1){
    header("Location: index.php?error=2");
    }else if($cliente -> getEstado() == 0){
        header("Location: index.php?error=3");
    }else{
        $_SESSION["id"] = $cliente -> getIdCliente();
        $_SESSION["rol"] = "Cliente";
        date_default_timezone_set('America/Bogota');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");

        $log = new Log("","InicioSesionCliente", $correo."&".$cliente -> getNombre()."&".$cliente -> getApellido(),$fecha,$hora,"2",$cliente -> getIdCliente());
        $log -> insertar();
        header("Location: index.php?pid=" . base64_encode("vista/sesionCliente.php"));
    }
  }else {
    header("Location: index.php?error=1");
  }
?>
