<?php
  $prov_prod = new Proveedor_producto("",$_SESSION["id"]);
  $arrayProduct = $prov_prod -> consultarprovee_prod();
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
      <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-header bg-warning ">
                <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de productos</h2>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                          <th>Foto</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                          foreach ($arrayProduct as $lpp) {
                              echo "<tr>";
                                echo "<td>".$i."</td>";
                                $idprod = $lpp -> getIdProducto_FK();
                                  $prod = new Producto($idprod);
                                  $prod -> consultarProducto();
                                echo "<td>".$prod -> getNombre()."</td>";
                                echo "<td>".$prod -> getPrecio()."</td>";
                                echo "<td>".$lpp -> getCantidad()." Lb.</td>";
                                echo "<td><img src='" . $prod -> getFoto() . "' height='70px'></td>";
                              echo "</tr>";
                              $i++;
                          }
                        ?>
                    </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
</div>
