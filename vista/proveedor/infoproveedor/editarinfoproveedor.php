<?php
  $img_nomb = "sinfotocliente.png";
  $destino = "img/imgproveedor/";
  $src = "";
  $tiempo = new DateTime();

  if(isset($_POST["crear"])){
    if($_FILES["foto"]["name"]  != ""){
      $auxfoto = $_FILES["foto"]["tmp_name"];
      $type_foto = $_FILES["foto"]["type"];
      $img_nomb = "img_".$_POST["nombre"].$tiempo -> getTimestamp().(($type_foto == "image/png")?".png":".jpg");
      $src = $destino.$img_nomb;
      copy($auxfoto,$src);
      $proveedor = new Proveedor($_SESSION["id"]);
      $proveedor -> consultar();

      if($proveedor -> getFoto() != "img/sinfotocliente.png"){
          unlink($proveedor -> getFoto());
      }

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Proveedor","Id:".$_SESSION["id"]."&Nombre:".$proveedor -> getNombre()."&Telefono:".$proveedor -> getTelefono()."&Direccion:".$proveedor -> getDireccion(),$fecha,$hora,"1",$_SESSION["id"]);
      $log -> insertar();


        $proveedor = new Proveedor($_SESSION["id"],$_POST["nombre"],$_POST["direccion"],$_POST["contraseña"],"",$_POST["telefono"],"",$src);
        $proveedor -> actualizar();

    }else {
      $proveedor = new Proveedor($_SESSION["id"]);
      $proveedor -> consultar();
      if($proveedor -> getFoto() != ""){
          $img_nomb = $proveedor -> getFoto();
      }
      $src = $img_nomb;

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Proveedor","Id:".$_SESSION["id"]."&Nombre:".$proveedor -> getNombre()."&Telefono:".$proveedor -> getTelefono()."&Direccion:".$proveedor -> getDireccion(),$fecha,$hora,"1",$_SESSION["id"]);
      $log -> insertar();

        $proveedor = new Proveedor($_SESSION["id"],$_POST["nombre"],$_POST["direccion"],$_POST["contraseña"],"",$_POST["telefono"],"",$src);
        $proveedor -> actualizar();
    }
  }else {
     $proveedor = new Proveedor($_SESSION["id"]);
     $proveedor -> consultar();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-warning">
                      <h2 style="font-family: 'Playfair Display', serif; font-size:30px">EDITAR PROVEEDOR</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("vista/proveedor/infoproveedor/editarinfoproveedor.php")?>" method="post" enctype="multipart/form-data">
                      <div class="row">
                          <div class="col-12 col-md-7">
                            <div class="form group">
                                <strong><label>CORREO</label></strong>
                                <input type="text" disabled="disabled" name="correo" class="form-control diseabled" value="<?php echo $proveedor -> getCorreo() ?>"  required>
                            </div>
                              <div class="form group">
                                  <strong><label>NOMBRE</label></strong>
                                  <input type="text" name="nombre" class="form-control" value="<?php echo $proveedor -> getNombre() ?>" required>
                              </div>
                              <div class="form group">
                                  <strong><label>TELEFONO</label></strong>
                                  <input type="number" name="telefono" class="form-control" value="<?php echo $proveedor -> getTelefono()?>" required>
                              </div>
                              <div class="form group">
                                  <strong><label>DIRECCION</label></strong>
                                  <input type="text" name="direccion" class="form-control" value="<?php echo $proveedor -> getDireccion() ?>" required>
                              </div>
                              <div class="form group">
                                  <strong><label>CONTRASEÑA</label></strong>
                                  <input type="password" name="contraseña" class="form-control" required>
                              </div>
                          </div>
                          <div class="col-12 col-md-5">
                            <div class="photo text-center" >
                                <strong><label for="foto">FOTO</label></strong>
                                      <div class="prevPhoto border border-dark">
                                      <span class="delPhoto notBlock border border-dark">X</span>
                                      <label for="foto"></label>
                                      </div>
                                      <div class="upimg border border-dark">
                                      <input type="file" name="foto" id="foto">
                                      </div>
                                      <div id="form_alert"></div>
                              </div>
                          </div>
                      </div>
                      <div class="dropdown-divider"></div>
                      <div class="form group">
                          <button type="submit" name="crear" class="form-control btn btn-warning">ACTUALIZAR DATOS</button>
                      </div>
                      <?php if(isset($_POST["crear"])){ ?>
                      <div class="dropdown-divider"></div>
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Actualizacion exitosamente!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                            </button>
                      </div>
                    <?php } ?>
                    </form>
                  </div>
              </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
  $("#foto").on("change",function(){
    var uploadFoto = document.getElementById("foto").value;
      var foto       = document.getElementById("foto").files;
      var nav = window.URL || window.webkitURL;
      var contactAlert = document.getElementById('form_alert');

          if(uploadFoto !='')
          {
              var type = foto[0].type;
              var name = foto[0].name;
              if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
              {
                  contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';
                  $("#img").remove();
                  $(".delPhoto").addClass('notBlock');
                  $('#foto').val('');
                  return false;
              }else{
                      contactAlert.innerHTML='';
                      $("#img").remove();
                      $(".delPhoto").removeClass('notBlock');
                      var objeto_url = nav.createObjectURL(this.files[0]);
                      $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                      $(".upimg label").remove();

                  }
            }else{
              alert("No selecciono foto");
              $("#img").remove();
            }
  });

  $('.delPhoto').click(function(){
    $('#foto').val('');
    $(".delPhoto").addClass('notBlock');
    $("#img").remove();

  });

});
</script>
