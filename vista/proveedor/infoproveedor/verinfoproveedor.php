<?php
  $proveedor = new Proveedor($_SESSION["id"]);
  $proveedor -> consultar();
?>
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-12 col-lg-8">
      <div class="card">
				<div class="card-header bg-warning">
					<h4 style="font-family: 'Playfair Display', serif; font-size:30px">INFORMACION PERSONAL</h4>
				    </div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-12 col-md-5">
              				<img src="<?php echo $proveedor -> getFoto()?>" width="100%" class="img-thumbnail">
              			</div>
              			<div class="col-12 col-md-7">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $proveedor -> getNombre() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $proveedor -> getCorreo() ?></td>
								</tr>
								<tr>
									<th>Direccion</th>
									<td><?php echo $proveedor -> getDireccion() ?></td>
								</tr>
                <tr>
									<th>Telefono</th>
									<td><?php echo $proveedor -> getTelefono() ?></td>
								</tr>
							</table>
						</div>
        	</div>
        </div>
      </div>
		</div>
	</div>
</div>
