<?php
  $inv = "";
  $produc = new Producto();
  $arrayprod = $produc -> consultarProductos();

  $prodselect  = "";
  if(isset($_POST["combo_prod"])){
    $prodselect  = $_POST["combo_prod"];
  }

  $cantidad  = "";
  if(isset($_POST["cantidad"])){
    $cantidad = $_POST["cantidad"];
  }

  if(isset($_POST["crear"])){

    $prov_prod = new Proveedor_producto($prodselect,$_SESSION["id"]);
    if($prov_prod -> verificar()){
      $inv = $prov_prod -> getCantidad() + $cantidad;
      $pp = new Proveedor_producto($prodselect,$_SESSION["id"],$inv);
      $pp -> actualizarCantidad();

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Cantidad","Id_Producto:".$prodselect."&Id_Proveedor:".$_SESSION["id"]."&Cantidad:".$cantidad,$fecha,$hora,"1",$_SESSION["id"]);
      $log -> insertar();
    }else{
      $pp = new Proveedor_producto($prodselect,$_SESSION["id"],$cantidad);
      $pp -> insertar();

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Agregar_Cantidad","Id_Producto:".$prodselect."&Id_Proveedor:".$_SESSION["id"]."&Cantidad:".$cantidad,$fecha,$hora,"1",$_SESSION["id"]);
      $log -> insertar();
    }

    $producto = new Producto($prodselect);
    $producto -> consultarProducto();
    $inv = $producto -> getInventario()+$cantidad;
    $prod = new Producto($prodselect,"","","","","","",$inv);
    $prod -> actualizarInventario();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header text-center bg-warning">
                      <h2 style="font-family: 'Fondamento', cursive; font-size:30px">PROVEER PRODUCTO</h2>
                  </div>
                  <div class="card-body">
                      <div class="row">
                            <div class="col-12">
                                <form class="" action="index.php?pid=<?php echo base64_encode("vista/proveedor/proveerprod.php")?>" method="post">
                                    <div class="form-group">
                                    <strong><label>PRODUCTOS</label></strong>
                                      <select class="form-control" name="combo_prod">
                                        <?php $i=1;
                                          foreach ($arrayprod as $lProduct) {
                                            echo "<option value=".$lProduct -> getIdProducto().">".$lProduct -> getNombre()."</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>
                                    <div class="form group">
                                        <strong><label>CANTIDAD</label></strong>
                                        <input type="number" name="cantidad" class="form-control" placeholder="# de cantidad en Lb.(Libras)" required>
                                    </div>
                                    <div class="dropdown-divider"></div>
                                    <div class="form group">
                                        <button type="submit" name="crear" class="form-control btn btn-warning">INGRESAR CANTIDAD</button>
                                    </div>
                                    <?php if(isset($_POST["crear"])){ ?>
                                    <div class="dropdown-divider"></div>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                      <strong>Cantidad registrada exitosamente!</strong>
                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                          </button>
                                    </div>
                                  <?php } ?>
                                </form>
                            </div>
                      </div>
                  </div>
              </div>
        </div>
    </div>

</div>
