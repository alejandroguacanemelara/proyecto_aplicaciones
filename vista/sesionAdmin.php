<?php
    $produc = new Producto();
    $cliente = new Cliente();
    $provee = new Proveedor();

?>
<div class="container mt-3 shadow-lg mb-5 rounded">
    <div class="row text-center d-flex justify-content-around">
        <div class="col-12 col-sm-2">
            <img src="img/imgadmin/iconoclienteadmin.png" width="110px">
            <h5><?php  echo $cliente -> consultarCantidad() ?> Clientes</h5>
        </div>
        <div class="col-12 col-sm-2">
            <img src="img/imgadmin/iconoproductoadmin.png" width="110px">
            <h5><?php echo $produc -> consultarCantidad() ?> Productos</h5>
        </div>
        <div class="col-12 col-sm-2">
            <img src="img/imgadmin/iconoproveedoradmin.png" width="110px">
            <h5><?php echo $provee -> consultarCantidad() ?> Proveedores</h5>
        </div>
    </div>
</div>
