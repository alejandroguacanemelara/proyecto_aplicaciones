<?php
  $admin = new Administrador($_SESSION["id"]);
  $admin -> consultar();
?>
<div class="container-fluid mt-3 bg-success">
        <div class="row">
            <div class="col text-center bg-success text-white" style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
            <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                La mejor tienda de frutas y vegetales
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-xl navbar-light bg-success">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("vista/sesionAdmin.php")?>"><img src="img/iconohome.png" width="25px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Producto <img src="img/productoicon.png" width="20px">
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/producto/regisproduc.php")?>">Ingresar Producto</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/producto/listaproduc.php")?>">Lista de productos</a>
          </div>
        </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Proveedor <img src="img/proveedoricon.png" width="20px">
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/proveedor/regisproveedor.php")?>">Ingresar Proveedor</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/proveedor/listaproveedor.php")?>">Lista de Proveedores</a>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Clientes <img src="img/clienteicon.png" width="20px">
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/cliente/listacliente.php")?>">Lista de Clientes</a>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Administrador <img src="img/iconmenuadmin.png" width="20px">
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/crearadmin.php")?>">Ingresar Administrador</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/listaadminis.php")?>">Lista de Administradores</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?pid=<?php echo base64_encode("vista/administrador/listaventas.php")?>" tabindex="-1" >Ventas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?pid=<?php echo base64_encode("vista/administrador/filtrolog.php")?>" tabindex="-1" >Log</a>
          </li>
      </ul>
      <ul class="navbar-nav">
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img src="<?php echo $admin -> getFoto() ?>" width="28px" class="rounded-circle"> Perfil Administrador <?php echo $admin -> getNombre(); ?></a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/infoadmin/verinfoadmin.php")?>">Ver perfil</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/administrador/infoadmin/editarinfoadmin.php")?>">Editar perfil</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="index.php?salir=true">Salir <span class="sr-only">(current)</span></a>
              </div>
          </li>
      </ul>
  </div>
</nav>
</div>
</div>
