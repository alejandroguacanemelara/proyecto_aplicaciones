<?php
  $provee = new Proveedor($_SESSION["id"]);
  $provee -> consultar();
?>
<div class="container">
  <div class="container mt-3 bg-warning">
      <div class="row">
            <div class="col text-center" style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
                <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                    La mejor tienda de frutas y vegetales
                </div>
            </div>
      </div>
  </div>
  <div>
    <nav class="navbar navbar-expand-md navbar-light bg-warning">
      <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("vista/sesionProveedor.php")?>"><img src="img/iconohome.png" width="30px"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Producto
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/proveedor/proveerprod.php") ?>">Proveer Producto</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/proveedor/listaproveeprod.php") ?>">Lista de Proveedores</a>
                </div>
              </li>

          </ul>
          <ul class="navbar-nav">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $provee -> getNombre() ?> <img src="<?php echo $provee -> getFoto() ?>" width="28px" class="rounded-circle"> </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/proveedor/infoproveedor/verinfoproveedor.php") ?>">Ver perfil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/proveedor/infoproveedor/editarinfoproveedor.php") ?>">Editar perfil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?salir=true">Salir <span class="sr-only">(current)</span></a>
                  </div>
              </li>
          </ul>
      </div>
    </nav>
  </div>
</div>
