<?php
  $img_nomb = "sinfotocliente.png";
  $destino = "img/imgcliente/";
  $src = "";
  $tiempo = new DateTime();

  $cliselect  = "";
  if(isset($_POST["combo_cli"])){
    $cliselect  = $_POST["combo_cli"];
  }
  if(isset($_POST["crear"])){
    if($_FILES["foto"]["name"]  != ""){
      $auxfoto = $_FILES["foto"]["tmp_name"];
      $type_foto = $_FILES["foto"]["type"];
      $img_nomb = "img_".$_POST["nombre"].$tiempo -> getTimestamp().(($type_foto == "image/png")?".png":".jpg");
      $src = $destino.$img_nomb;
      copy($auxfoto,$src);
      $cliente = new Cliente($_SESSION["id"]);
      $cliente -> consultar();

      if($cliente -> getFoto() != "img/sinfotocliente.png"){
          unlink($cliente -> getFoto());
      }

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Cliente","Id:".$_SESSION["id"]."&Nombre:".$cliente -> getNombre()."&Apellido:".$cliente -> getApellido()."&Correo:".$cliente -> getCorreo()."&Sexo:".$cliente -> getSexo()."&Direccion:".$cliente -> getDireccion(),$fecha,$hora,"2",$_SESSION["id"]);
      $log -> insertar();

      if($cliselect == ""){
        $cliente = new Cliente($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],"",$src,"","",$_POST["direccion"]);
        $cliente -> actualizar();
      }else {
        $cliente = new Cliente($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],"",$src,$cliselect,"",$_POST["direccion"]);
        $cliente -> actualizar();
      }

    }else {
      $cliente = new Cliente($_SESSION["id"]);
      $cliente -> consultar();
      if($cliente -> getFoto() != ""){
          $img_nomb = $cliente -> getFoto();
      }
      $src = $img_nomb;

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Cliente","Nombre:".$cliente -> getNombre()."&Apellido:".$cliente -> getApellido()."&Correo:".$cliente -> getCorreo()."&Sexo:".$cliente -> getSexo()."&Direccion:".$cliente -> getDireccion(),$fecha,$hora,"2",$_SESSION["id"]);
      $log -> insertar();

      if($cliselect == ""){
        $cliente = new Cliente($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],"",$src,"","",$_POST["direccion"]);
        $cliente -> actualizar();
      }else {
        $cliente = new Cliente($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],"",$src,$cliselect,"",$_POST["direccion"]);
        $cliente -> actualizar();
      }
    }
  }else {
     $cliente = new Cliente($_SESSION["id"]);
     $cliente -> consultar();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center text-white" style="background-color: #000000;">
                      <h2 style="font-family: 'Playfair Display', serif; font-size:30px">EDITAR INFORMACION DEL CLIENTE</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("vista/cliente/infocliente/editarinfocliente.php")?>" method="post" enctype="multipart/form-data">
                      <div class="row d-flex justify-content-center">
                        <div class="col-12 col-md-7">
                          <div class="form group">
                                    <strong><label>CORREO</label></strong>
                                    <input type="text" disabled="disabled" name="correo" class="form-control diseabled" value="<?php echo $cliente -> getCorreo() ?>"  required>
                                </div>
                                  <div class="form group">
                                      <strong><label>NOMBRE</label></strong>
                                      <input type="text" name="nombre" class="form-control" value="<?php echo $cliente -> getNombre() ?>" required>
                                  </div>
                                  <div class="form group">
                                      <strong><label>APELLIDO</label></strong>
                                      <input type="text" name="apellido" class="form-control" value="<?php echo (($cliente -> getApellido()!= "")?$cliente -> getApellido():"") ?>" required>
                                  </div>
                                  <div class="form group">
                                    <strong><label>SEXO</label></strong>
                                    <select class="form-control text-dark" name="combo_cli">
                                        <option value="0">MASCULINO</option>
                                        <option value="1">FENENIMO</option>
                                        <option value="2">NO DEFINIDO</option>
                                    </select>
                                  </div>
                                  <div class="form group">
                                      <strong><label>DIRECCION</label></strong>
                                      <input type="text" name="direccion" class="form-control" value="<?php echo (($cliente -> getDireccion()!= "")?$cliente -> getDireccion():"") ?>" required>
                                  </div>
                                  <div class="form group">
                                      <strong><label>CONTRASEÑA</label></strong>
                                      <input type="password" name="contraseña" class="form-control" required>
                                  </div>
                        </div>
                        <div class="col-12 col-md-4">
                          <div class="photo text-center" >
                              <strong><label for="foto">FOTO</label></strong>
                                    <div class="prevPhoto">
                                    <span class="delPhoto notBlock">X</span>
                                    <label for="foto"></label>
                                    </div>
                                    <div class="upimg">
                                    <input type="file" name="foto" id="foto">
                                    </div>
                                    <div id="form_alert"></div>
                            </div>
                        </div>
                      </div>
                      <div class="dropdown-divider"></div>
                      <div class="form group">
                          <button type="submit" name="crear" class="form-control btn btn-dark">ACTUALIZAR DATOS</button>
                      </div>
                      <?php if(isset($_POST["crear"])){ ?>
                      <div class="dropdown-divider"></div>
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Actualizacion exitosamente!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                            </button>
                      </div>
                    <?php } ?>
                    </form>
                  </div>
              </div>
        </div>
    </div>

</div>


<script>
$(document).ready(function(){
  $("#foto").on("change",function(){
    var uploadFoto = document.getElementById("foto").value;
      var foto       = document.getElementById("foto").files;
      var nav = window.URL || window.webkitURL;
      var contactAlert = document.getElementById('form_alert');

          if(uploadFoto !='')
          {
              var type = foto[0].type;
              var name = foto[0].name;
              if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
              {
                  contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';
                  $("#img").remove();
                  $(".delPhoto").addClass('notBlock');
                  $('#foto').val('');
                  return false;
              }else{
                      contactAlert.innerHTML='';
                      $("#img").remove();
                      $(".delPhoto").removeClass('notBlock');
                      var objeto_url = nav.createObjectURL(this.files[0]);
                      $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                      $(".upimg label").remove();

                  }
            }else{
              alert("No selecciono foto");
              $("#img").remove();
            }
  });

  $('.delPhoto').click(function(){
    $('#foto').val('');
    $(".delPhoto").addClass('notBlock');
    $("#img").remove();

  });

});
</script>
