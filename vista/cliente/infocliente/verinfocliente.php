<?php
  $cliente = new Cliente($_SESSION["id"]);
  $cliente -> consultar();

  $sexo = $cliente -> getSexo();

  if($sexo != ""){
    if($sexo == 1){
        $sexo = "Femenino";
    }elseif ($sexo == 0) {
        $sexo = "Masculino";
    }else {
        $sexo = "No definido";
    }
  }else {
    $sexo = "Sin datos";
  }
?>
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-12 col-md-8">
            <div class="card">
				<div class="card-header text-white" style="background-color: #000000;">
					<h4 style="font-family: 'Playfair Display', serif; font-size:30px">INFORMACION PERSONAL</h4>
				</div>
              	<div class="card-body">
              		<div class="row">
              			<div class="col-12 col-sm-5">
              				<img src="<?php echo ($cliente -> getFoto() != "")?$cliente -> getFoto():"img/sinfotocliente.png"; ?>" width="100%" class="img-thumbnail">
              			</div>
              			<div class="col-12 col-sm-7">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $cliente -> getNombre() ?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $cliente -> getApellido() ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $cliente -> getCorreo() ?></td>
								</tr>
                <tr>
									<th>Sexo</th>
									<td><?php echo $sexo ?></td>
								</tr>
                <tr>
									<th>Direccion</th>
									<td><?php echo $cliente -> getDireccion() ?></td>
								</tr>
							</table>
						</div>
              		</div>
            	</div>
            </div>
		</div>
	</div>
</div>
