<?php
  $producto = new Producto();

  if(isset($_POST["crear"])){
      $idprod = $_GET["idProducto"];
      $cantidad = $_POST["cantidad"];
      $band = false;
      $auxarray = array();
      $auxarray1 = array();

      $auxarray = $_SESSION["carrito"];
      $auxarray1 = $_SESSION["cantidad"];

      for ($i=0; $i < count($auxarray) ; $i++) {
        if($idprod == $auxarray[$i]){
            $band = true;
        }
      }

     if($band == false){
        array_push($auxarray, $idprod);
        array_push($auxarray1, $cantidad);

        $_SESSION["carrito"] = $auxarray;
        $_SESSION["cantidad"] = $auxarray1;

        $caso = 0;
      }else {
        $caso = 1;
      }
    }

  $cantidad = 8;
  if (isset($_GET["cantidad"])) {
      $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if (isset($_GET["pagina"])) {
      $pagina = $_GET["pagina"];
  }
  $filtro = "";
  if (isset($_GET["filtro"])) {
      $filtro = $_GET["filtro"];
  }
  $cant = $producto->consultarCantidad_filtro($filtro);
  $cantPagina = intval($cant[0] / $cantidad);
  if (($cant[0] % $cantidad) != 0) {
      $cantPagina++;
  }
  $arrayprod = $producto -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>

<div id="contenido">
  <section class="catalogo d-flex flex-wrap">
    <?php foreach ($arrayprod as $prod) {
                if($prod -> getInventario() != 0){
      ?>
      <div class="col-12 d-flex justify-content-around col-md-5 col-lg-3 mt-3">
        <div class="card" style="width: 15rem;">
          <img src="<?php echo $prod -> getFoto() ?>" class="card-img-top"  width="300px">
          <div class="card-body">
            <h5 class="card-title"><?php echo $prod -> getNombre() ?></h5>
            <p class="card-text">$ <?php echo $prod -> getPrecio() ?></p>
            <p class="card-text"> Disponible: <?php echo $prod -> getInventario() ?> Lb.</p>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalcantidad<?php echo $prod -> getIdProducto() ?>">Añadir al carrito</button>
          </div>
        </div>
      </div>
    <?php }
    } ?>
  </section>
  <div class="d-flex justify-content-end">
      <nav>
          <ul class="pagination">
              <?php if ($pagina > 1) {
                  echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("vista/cliente/catalogoProductos.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
              } ?>
              <?php for ($i = 1; $i <= $cantPagina; $i++) {
                  if ($pagina == $i) {
                      echo "<li class='page-item active'>" .
                          "<a class='page-link'>$i</a>" .
                          "</li>";
                  } else {
                      echo "<li class='page-item'>" .
                          "<a class='page-link' href='index.php?pid=" . base64_encode("vista/cliente/catalogoProductos.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                          "</li>";
                  }
              } ?>
              <?php if ($pagina < $cantPagina) {
                  echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("vista/cliente/catalogoProductos.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
              } ?>
          </ul>
      </nav>
  </div>
</div>



<!-- Funcion para mostrar un mensaje de la variable error -->
<script>
    $(document).ready(function(){
      $("#Modalerror").modal("show");
    });
</script>

<!-- Modal Cantidad -->
<?php foreach ($arrayprod as $prod){ ?>
  <div class="modal fade" id="modalcantidad<?php echo $prod -> getIdProducto() ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" action="index.php?pid=<?php echo base64_encode("vista/cliente/catalogoProductos.php")?>&idProducto=<?php echo $prod -> getIdProducto() ?>" method="post">
            <div class="form-group">
              <h4 style="font-family: 'MuseoModerno', cursive;font-family: 'Noto Serif', serif;">Cantidad del producto <?php echo $prod -> getNombre() ?></h4>
              <input class="form-control" type="number" min="1" max="<?php echo $prod -> getInventario() ?>"name="cantidad" placeholder="# de cantidad en Lb. (Libras)"required>
            </div>
            <div class="dropdown-divider"></div>
            <div class="form group">
                <button type="submit" name="crear" class="form-control btn btn-info">Añadir al carrito</button>
            </div>
          </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<?php } ?>


<!-- Modal Error -->
<?php if(isset($caso)){ ?>
<div class="modal" id="Modalerror" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header
        <?php if($caso==1){
                echo "bg-danger";
        }else{
                echo "bg-success";
        } ?>
      ">
        <?php if($caso == 1){ ?>
        <div>
          <strong><h5 class="modal-title">Producto en el carrito duplicado.</h5></strong>
        </div>
      <?php }else if($caso == 0){?>
        <div>
          <strong><h5 class="modal-title">Producto agregado al carrito exitosamente.</h5></strong>
        </div>
      <?php } ?>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
</div>
<?php } ?>


<style>
    .catalogo{
      justify-content-center;
    }
</style>
