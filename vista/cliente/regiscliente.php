<?php
  $error = 0;
  $registro = false;
  if(isset($_POST["crear"])){
    $nombre = $_POST["nombre"];
    $correo = $_POST["correo"];
    $contra = $_POST["contraseña"];
    $foto = "img/sinfotocliente.png";
    $cliente = new Cliente("",$nombre,"",$contra,$correo,$foto);

    if($cliente -> existeCorreo()){
        $error= 1;
    }else {
      $url = $cliente -> insertar();
      $verifCli = new Cliente("","","","",$correo);
      $verifCli -> consultarid();
      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Crear_cliente","Nombre:".$nombre."&Correo:".$correo,$fecha,$hora,"2",$verifCli -> getIdCliente());
      $log -> insertar();
      $registro = true;
    }
  }
?>
<div class="container-fluid mt-3" style="background-color: #000000; opacity: 0.8;">
    <div class="row">
          <div class="col-2 text-center allign-middle mt-3">
            <a href="index.php"> <img src="img/volver.png" width="45px"></a>
          </div>
          <div class="col-8 text-center text-white">
            <div style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
            </div>
            <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                La mejor tienda de frutas y vegetales
            </div>
          </div>
    </div>
</div>
<div class="container mt-3">
    <div class="row">
      <div class="col-lg-3 col-md-0"></div>
      <div class="col-lg-7 col-md-12" >
          <div class="card text-center">
            <div class="card-header text-center text-white" style="background-color: #000000;">
              <h2>Acceder</h2>
            </div>
            <div class="card-body" style="opacity: 0.8;">
              <div class="row">
                <div class="col-6">
                  <form class="" action="index.php?pid=<?php echo base64_encode("vista/cliente/regiscliente.php")?>" method="post">
                    <div class="form group">
                      <strong><label>NOMBRE *</label></strong>
                      <input type="text" name="nombre" class="form-control" required>
                    </div>
                    <div class="form group">
                      <strong><label>CORREO ELECTRONICO *</label></strong>
                      <input type="email" name="correo" class="form-control" required>
                    </div>
                    <div class="form-group">
                      <strong><label>CONTRASEÑA *</label></strong>
                      <input type="password" name="contraseña" class="form-control" required>
                    </div>
                    <div class="dropdown-divider"></div>
                    <div class="form group">
                      <button style="background-color: #000000;" type="submit" name="crear" class="form-control btn btn-secondary">Registrarse</button>
                    </div>
                    <?php if($error == 1){ ?>
                    <div class="dropdown-divider"></div>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Cuenta <?php echo $correo ?>, existente!</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                  <?php }else if($registro){?>
                    <div class="dropdown-divider"></div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <strong>Cuenta creada exitosamente!</strong>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                  <?php } ?>
                  </form>
                </div>
                <div class="col-3 text-center">
                   <div>
                      <img src="img/regiscliente.jpg" width="200%">
                  </div>
               </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div background="#000000" class="text-center">
      <?php if($registro){ ?>
        LINK PARA ACTIVACION: <?php echo $url ?>
      <?php } ?>
    </div>
</div>
