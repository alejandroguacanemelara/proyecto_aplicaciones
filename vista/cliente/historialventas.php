<?php
  $venta = new Venta("",$_SESSION["id"]);

  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayVenCli = $venta -> consultarVentasCliPaginacion($cantidad, $pagina);
  $totalRegistros = $venta -> consultarCantidadVenCli();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
  <div class="row d-flex justify-content-center">
    <div class="col-12 col-lg-8">
      <div class="card">
          <div class="card-header text-white" style="background-color: #000000;">
              <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Registro de compras</h2>
          </div>
          <div class="card-body">
              <table class="table table-responsive-lg">
                  <thead class="table-secondary">
                      <tr>
                        <th>#</th>
                        <th>Referecia</th>
                        <th>Precio</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>-</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i=1;
                        foreach ($arrayVenCli as $lVen) {
                            echo "<tr>";
                              echo "<td>".$i."</td>";
                              echo "<td>".$lVen -> getId_venta()."</td>";
                              echo "<td>$ ".$lVen -> getPrecio_ven()."</td>";
                              echo "<td>".$lVen -> getFecha_ven()."</td>";
                              echo "<td>".$lVen -> getHora_ven()."</td>";
                        ?>
                              <td><button class="border border-light" type="button" data-toggle="modal" data-target="#modalventa<?php echo $lVen -> getId_venta() ?>"> <i class="fas fa-eye"></i></button></td>
                        <?php
                            echo "</tr>";
                            $i++;
                        }
                      ?>
                  </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <nav>
                  <ul class="pagination">
                    <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/cliente/historialventas.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                    <?php
                    for($i=1; $i<=$totalPaginas; $i++){
                        if($i==$pagina){
                            echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                        }else{
                            echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("vista/cliente/historialventas.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                        }
                    }
                    ?>
                    <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/cliente/historialventas.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                  </ul>
                </nav>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<?php foreach ($arrayVenCli as $lVen){ ?>
  <div class="modal fade" id="modalventa<?php echo $lVen -> getId_venta() ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header ">
            <h4 style="font-family: 'Playfair Display', serif; font-size:25px">PRODUCTOS DE LA VENTA</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body d-flex flex-sm-wrap">
            <?php
                $prod_ven = new Prod_ven($lVen -> getId_venta());
                $arrayprod_ven = $prod_ven -> consultarVentas_v();

                foreach ($arrayprod_ven as $lprodven) {
                  $producto = new Producto($lprodven -> getId_productofk());
                  $producto -> consultarProducto();
                  ?>
                  <div class="card col-6" style="width: 5rem;">
                    <img src="<?php echo $producto -> getFoto() ?>" class="card-img-top"  width="100px">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $producto -> getNombre() ?> - <?php echo $lprodven -> getCantidad() ?> Lb. </h5>
                    </div>
                  </div>
            <?php
                }
             ?>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<?php } ?>
