<?php
  $correo = $_GET["correo"];
  $codigoActivacion = base64_decode($_GET["codigoActivacion"]);
  $cliente = new Cliente("","","","",$correo);
  $resul = $cliente -> activarCliente($codigoActivacion);
?>
<div class="container-fluid mt-3" style="background-color: #000000; opacity: 0.8;">
    <div class="row">
          <div class="col-2 text-center allign-middle">
            <a href="index.php"> <img src="img/retroceder.png" width="45px"></a>
          </div>
          <div class="col-8 text-center text-white">
            <div style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
            </div>
            <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                La mejor tienda de frutas y vegetales
            </div>
          </div>
    </div>
</div>
<div class="container mt-3">
    <div class="row">
      <div class="col-lg-3 col-md-0"></div>
      <div class="col-lg-7 col-md-12" >
          <div class="card text-center">
            <div class="card-header text-center text-white" style="background-color: #000000;">
              <h2>Acceder</h2>
            </div>
            <div class="card-body" style="opacity: 0.8;">
              <div class="row">
                <div class="col-3 text-center">
                   <div>
                      <img src="img/welcome.jpg" width="200%">
                  </div>
               </div>
                <?php if($resul){ ?>
      					<div class="alert alert-success alert-dismissible fade show" role="alert">
      						La cuenta con el correo <?php echo $correo ?> ha sido activada.
      						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      					</div>
      					<?php } else { ?>
      					<div class="alert alert-danger alert-dismissible fade show" role="alert">
      						La cuenta con el correo <?php echo $correo ?> NO ha sido activada. Revise su correo de activacion
      						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      					</div>
      					<?php } ?>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
