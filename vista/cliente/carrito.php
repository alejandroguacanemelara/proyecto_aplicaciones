<?php
  $sumatoria = 0;
  $band = false;
  $indice = 0;

  if(isset($_GET["idProducto"])){
    $idprod = $_GET["idProducto"];

    for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {
      if($idprod == $_SESSION["carrito"][$i]){
        $band = true;
        $indice = $i;
      }
    }

    if($band == true){
      $_SESSION["carrito"][$indice] = "vacio";
      $_SESSION["cantidad"][$indice] = "vacio";
    }
  }

  $cont = 0;
  if($_SESSION["carrito"] != ""){
    for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {
       if($_SESSION["carrito"][$i] == "vacio"){
         $cont++;
       }
    }
  }

?>
<div class="container mt-3">
  <div class="row">
      <div class="col-12 col-md-8">
        <section class="catalogo d-flex flex-wrap">
          <?php
            if(!empty($_SESSION["carrito"])){
              for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {

                  if($_SESSION["carrito"][$i] != "vacio"){
                    $producto = new Producto($_SESSION["carrito"][$i]);
                    $producto -> consultarProducto();
                    ?>
                    <div class="col-6 d-flex justify-content-around col-lg-4  mt-3">
                      <div class="card" style="width: 13rem;">
                        <img src="<?php echo $producto -> getFoto() ?>" class="card-img-top"  width="100px" >
                        <div class="card-body">
                          <h5 class="card-title"><?php echo $producto -> getNombre() ?></h5>
                          <p class="card-text">Cantidad <?php echo $_SESSION["cantidad"][$i] ?> Lb.</p>
                          <a href="index.php?pid=<?php echo base64_encode("vista/cliente/carrito.php")?>&idProducto=<?php echo $producto -> getIdProducto() ?>"  class="btn btn-danger">Eliminar del carrito</a>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
            }
          }elseif (empty($_SESSION["carrito"]) || $cont == count($_SESSION["carrito"])) {
            ?>
              <div>
                  <h1>No hay productos seleccionados.</h1>
              </div>
            <?php
          }?>
        </section>
      </div>
      <div class="col-12 col-md-4 d-flex align-items-baseline d-flex justify-content-around">
         <div class="card ">
              <div class="card-header text-white" style="background-color: #000000;">
                  <h3>Detalles de la compra</h3>
              </div>
              <div class="card-body">
                  <?php
                  if(!empty($_SESSION["carrito"])){
                    for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {

                        if($_SESSION["carrito"][$i] != "vacio"){
                          $producto = new Producto($_SESSION["carrito"][$i]);
                          $producto -> consultarProducto();

                          $sumatoria = $sumatoria + ($_SESSION["cantidad"][$i]*$producto -> getPrecio());

                  ?>
                  <div class="d-flex flex-justify-content-end flex-column bd-highlight">
                      <div class="p-2 bd-highlight"><?php echo $producto -> getNombre() ?><br>$ <?php echo $_SESSION["cantidad"][$i]*$producto -> getPrecio()?>.....(<?php echo $_SESSION["cantidad"][$i] ?> Lb.) * <?php echo $producto -> getPrecio()?></div>
                  </div>
                <?php
                        }
                    } ?>
                  <div class="" style="border-color: #000000;">
                     <h5 class="d-flex justify-content-end">
                       <strong>TOTAL: </strong> $<?php echo $sumatoria ?>
                     </h5>
                  </div>
                  <div class="dropdown-divider"></div>
                  <a href="index.php?pid=<?php echo base64_encode("vista/cliente/catalogoProductos.php")?>&caso=2"  class="btn text-white
               <?php
                      if ($cont == count($_SESSION["carrito"]) || empty($_SESSION["carrito"])) {
                        echo "disabled";
                      }
                   ?>" style="background-color: #000000;">Realizar compra</a>
                <?php
                  } ?>
              </div>
         </div>
      </div>
  </div>
</div>

<style>
    .catalogo{
      justify-content: between;
    }
</style>
