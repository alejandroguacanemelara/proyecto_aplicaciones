<?php

    if (isset($_GET["caso"])) {
        $sumatoria = 0;
        $id_clientefk = $_SESSION["id"];
        $tiempo = new DateTime();
        $id_venta = rand(1000,9999).$tiempo -> getTimestamp().rand(1000,9999);

        for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {
            if($_SESSION["carrito"][$i] != "vacio"){
              $producto = new Producto($_SESSION["carrito"][$i]);
              $producto -> consultarProducto();
              $sumatoria = $sumatoria + ($_SESSION["cantidad"][$i]*$producto -> getPrecio());
            }
        }

        date_default_timezone_set('America/Bogota');
        $fecha = date("Y-m-d");
        $hora = date("H:i:s");
        $log = new Log("","Crear_venta",$id_venta,$fecha,$hora,"2",$_SESSION["id"]);
        $log -> insertar();

        $venta = new Venta($id_venta,$id_clientefk,$sumatoria,$fecha,$hora);
        $venta -> insertar();

        for ($i=0; $i < count($_SESSION["carrito"]) ; $i++) {
            if($_SESSION["carrito"][$i] != "vacio"){
                $id_prod = $_SESSION["carrito"][$i];
                $cantidad = $_SESSION["cantidad"][$i];

                $prod_venta = new Prod_ven($id_venta,$id_prod,$cantidad);
                $prod_venta -> insertar();

                $producto = new Producto($id_prod);
                $producto -> consultarProducto();
                $inv = $producto -> getInventario()-$cantidad;
                $prod = new Producto($id_prod,"","","","","","",$inv);
                $prod -> actualizarInventario();
            }
        }
        $_SESSION["carrito"] = array();
        $_SESSION["cantidad"] = array();
    }
  $cliente = new Cliente($_SESSION["id"]);
  $cliente -> consultar();
  $info = 0;
  if($cliente -> getApellido() != ""){
    $info++;
  }
  if($cliente -> getFoto() != "img/sinfotocliente.png"){
    $info++;
  }
  if($cliente -> getSexo() != ""){
    $info++;
  }
  if($cliente -> getDireccion() != ""){
    $info++;
  }
  $porcentaje = ($info/4)*100;

?>
<div class="container"  style="background-color: #000000;">
  <div class="container mt-3 ">
      <div class="row">
            <div class="col text-center  text-white" style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
                <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                    La mejor tienda de frutas y vegetales
                </div>
            </div>
      </div>
  </div>
  <div>
    <nav class="navbar navbar-expand-md navbar-light">
      <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("vista/sesionCliente.php")?>"><img src="img/iconohomecliente.png" width="30px"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span>
          <img src="img/icondesplegable.png" width="30px">
        </span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("vista/cliente/catalogoProductos.php") ?>" tabindex="-1" ><i class="fa fa-shopping-bag" aria-hidden="true"></i> Lista Productos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("vista/cliente/carrito.php") ?>" tabindex="-1" ><i class="fa fa-shopping-cart" aria-hidden="true"></i> Carrito</a>
              </li>

          </ul>
          <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link text-white" href="index.php?pid=<?php echo base64_encode("vista/cliente/historialventas.php") ?>" tabindex="-1" ><i class="fa fa-history" aria-hidden="true"></i> Historial</a>
                </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $cliente -> getNombre()." " ?> <img src="<?php echo $cliente -> getFoto() ?>" width="28px" class="rounded-circle"></a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/cliente/infocliente/verinfocliente.php") ?>">Ver perfil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("vista/cliente/infocliente/editarinfocliente.php") ?>">Editar perfil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?salir=true">Salir <span class="sr-only">(current)</span></a>
                  </div>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white" href="#" tabindex="-1" ><?php echo $porcentaje."%" ?></a>
              </li>
          </ul>
      </div>
    </nav>
  </div>
</div>



<!-- Modal Error -->
<?php if(isset($_GET["caso"])){ ?>
<div class="modal" id="Modalerror" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header
        <?php if($_GET["caso"]==2){
                echo "bg-success";
        }else{
                echo "bg-success";
        } ?>
      ">
        <?php if($_GET["caso"]==2){ ?>
        <div>
          <strong><h5 class="modal-title">Venta realizada correctamente.</h5></strong>
        </div>
      <?php } ?>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>
  </div>
</div>
<?php } ?>
