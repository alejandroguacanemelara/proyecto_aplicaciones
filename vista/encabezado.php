<div class="container-fluid"  style="background-color: #000000;">
    <div class="row d-flex justify-content-center">
          <div class="col-2 col-lg-4 text-center allign-middle mt-2">
            <a href="index.php"> <img src="img/productoicon.png" width="50px"></a>
          </div>
          <div class="col-6 col-lg-4 text-center text-white">
            <div style="font-family: 'Lobster', cursive; font-size:36px">
                FRUVER.NET
            </div>
            <div style="font-family: 'MuseoModerno', cursive; font-size: 16px">
                La mejor tienda de frutas y vegetales
            </div>
          </div>
          <div class="col-2 d-flex justify-content-center mt-3  col-lg-4 text-center mt-3 ">
            <div>
              <button class="btn btn-outline-light" type="button" data-toggle="modal" data-target="#modalIngresar" name="ingresar" style="font-family: 'Lobster', cursive; font-size:17px">Ingresar</button>
              <a href="index.php?pid=<?php echo base64_encode("vista/cliente/regiscliente.php") ?>"><button class="btn btn-outline-light" type="button" name="Registrarse" style="font-family: 'Lobster', cursive; font-size:16px">Registrarse</button></a>
            </div>
          </div>
    </div>
</div>
<!-- Modal Ingresar -->
<div class="modal fade" id="modalIngresar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-family: 'MuseoModerno', cursive; font-size: 30px">Iniciar Sesion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex justify-content-center">

        <div class="col-10">
          <form class="" action="index.php?pid=<?php echo base64_encode("vista/autenticacion.php") ?>" method="post">
            <div class="form-group">
              <h4 style="font-family: 'Lobster', cursive; font-size:20px">Correo</h4>
              <input class="form-control" type="email" name="correo" required>
            </div>
            <div class="form-group">
              <h4 style="font-family: 'Lobster', cursive; font-size:20px">Contraseña</h4>
              <input class="form-control" type="password" name="contraseña" required>
            </div>
            <div class="dropdown-divider"></div>
            <div class="form group">
                <?php  ?>
                <button type="submit" name="iniciar" class="form-control btn btn-outline-dark" style="font-family: 'Lobster', cursive; font-size:16px; border-color: #000000;">Iniciar sesión</button>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<!-- Funcion para mostrar un mensaje de la variable error -->
<script>
    $(document).ready(function(){
      $("#Modalerror").modal("show");
    });
</script>


<!-- Modal Error -->
<?php if(isset($_GET["error"])){ ?>
<div class="modal" id="Modalerror" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-danger">
        <h5 class="modal-title">ERROR</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php if(isset($_GET["error"]) && $_GET["error"] == 1){ ?>
        <div>
          <strong>Correo o contraseña no valida.</strong>
        </div>
      <?php }else if(isset($_GET["error"]) && $_GET["error"] == 2){?>
        <div>
          <strong>Su cuenta esta bloqueada</strong>
        </div>
      <?php }else if(isset($_GET["error"]) && $_GET["error"] == 3){?>
        <div>
          <strong>Su cuenta esta inactiva</strong>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<!-- Modal Registrar -->
<div class="modal fade" id="modalRegistrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel" style="font-family: 'MuseoModerno', cursive;font-family: 'Noto Serif', serif; font-size:36px">Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="index.php?pid=<?php echo base64_encode("vista/autenticacion.php") ?>" method="post">
          <div class="form-group">
            <h4 style="font-family: 'MuseoModerno', cursive;font-family: 'Noto Serif', serif;">Nombre</h4>
            <input class="form-control" type="text" name="correo" required>
            <small class="form-text text-muted">Introduzca su nombre</small>
          </div>
          <div class="form-group">
            <h4 style="font-family: 'MuseoModerno', cursive;font-family: 'Noto Serif', serif;">Correo</h4>
            <input class="form-control" type="email" name="correo" required>
            <small class="form-text text-muted">Introduzca un correo valido</small>
          </div>
          <div class="form-group">
            <h4 style="font-family: 'MuseoModerno', cursive; font-family: 'Noto Serif', serif;">Contraseña</h4>
            <input class="form-control" type="password" name="contraseña" required>
            <small class="form-text text-muted">Introduzca la contraseña</small>
          </div>
          <div class="dropdown-divider"></div>
          <div class="form group">
              <button type="submit" name="iniciar" class="form-control btn btn-success" style="font-family: 'Lobster', cursive; font-size:16px">Registrarse</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
