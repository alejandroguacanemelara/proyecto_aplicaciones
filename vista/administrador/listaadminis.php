<?php
  $admin = new Administrador();
  $arrayAdmin = $admin -> consultarAdministradores();
?>
<div class="container mt-3">
      <div class="card">
          <div class="card-header bg-success text-white">
              <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de administradores</h2>
          </div>
          <div class="card-body">
              <table class="table table-responsive-lg">
                  <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Identificacion</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Foto</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i=1;
                        foreach ($arrayAdmin as $lAdmin) {
                            echo "<tr>";
                              echo "<td>".$i."</td>";
                              echo "<td>".$lAdmin -> getIdAdministrador()."</td>";
                              echo "<td>".$lAdmin -> getNombre()."</td>";
                              echo "<td>".$lAdmin -> getApellido()."</td>";
                              echo "<td>".$lAdmin -> getCorreo()."</td>";
                              echo "<td><img src='" . $lAdmin -> getFoto() . "' height='45px'></td>";
                            echo "</tr>";
                            $i++;
                        }
                      ?>
                  </tbody>
            </table>
          </div>
      </div>
</div>
