<?php
  $producto = new Producto();
  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayProduct = $producto -> consultarPaginacion($cantidad, $pagina);
  $totalRegistros = $producto -> consultarCantidad();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3 d-flex justify-content-center">
  <div class="col-12 col-lg-9">
    <div class="card">
        <div class="card-header bg-success text-white">
            <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de productos</h2>
        </div>
        <div class="card-body">
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                    <tr>
                      <th>#</th>
                      <th>Nombre</th>
                      <th>Precio</th>
                      <th>Fecha registro</th>
                      <th>Inventario</th>
                      <th>Foto</th>
                      <th>_</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i=1;
                      foreach ($arrayProduct as $lProduct) {
                          echo "<tr>";
                            echo "<td>".$i."</td>";
                            echo "<td>".$lProduct -> getNombre()."</td>";
                            echo "<td>$ ".$lProduct -> getPrecio()."</td>";
                            echo "<td>".$lProduct -> getFech_reg()."</td>";
                            echo "<td>".$lProduct -> getInventario()." Lb.</td>";
                            ?>
                            <td><button class="border border-light" type="button" data-toggle="modal" data-target="#modalventa<?php echo $lProduct -> getIdProducto() ?>"> <img src="<?php echo $lProduct -> getFoto() ?>" height='35px'></button></td>
                            <?php

                            echo "<td><a href='index.php?pid=". base64_encode("vista/administrador/producto/editarproduc.php") . "&idProducto=" . $lProduct -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";
                          echo "</tr>";
                          $i++;
                      }
                    ?>
                </tbody>
          </table>
          <div class="d-flex justify-content-end">
              <nav>
                <ul class="pagination">
                  <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/producto/listaproduc.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                  <?php
                  for($i=1; $i<=$totalPaginas; $i++){
                      if($i==$pagina){
                          echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                      }else{
                          echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("vista/administrador/producto/listaproduc.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                      }
                  }
                  ?>
                  <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/producto/listaproduc.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                </ul>
              </nav>
            </div>
        </div>
    </div>
  </div>
</div>


<?php foreach ($arrayProduct as $lProduct){ ?>
  <div class="modal fade" id="modalventa<?php echo $lProduct -> getIdProducto()?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h3 style="font-family: 'Playfair Display', serif; font-size:30px" > <?php echo $lProduct -> getNombre() ?></h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="d-flex justify-content-center">
                <img class="rounded-circle" src="<?php echo $lProduct -> getFoto() ?>" width="200px">
            </div>
            <div class="dropdown-divider"></div>
            <div class="row d-flex justify-content-center">
                <div class="col-4 ">
                    <p><strong>Precio:</strong><br>$ <?php echo $lProduct -> getPrecio() ?></p>
                    <p><strong>Fecha registro</strong> <br><?php echo $lProduct -> getFech_reg() ?></p>
                </div>
                <div class="col-6">
                    <p><strong>Descripcion:</strong><br> <?php echo $lProduct -> getDescripcion() ?></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<?php } ?>
