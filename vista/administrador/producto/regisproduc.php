<?php
  $nombre = "";
  if(isset($_POST["nombre"])){
        $nombre = $_POST["nombre"];
  }

  $precio = "";
  if(isset($_POST["precio"])){
        $precio = $_POST["precio"];
  }

  $descripcion = "";
  if(isset($_POST["descripcion"])){
    $descripcion = $_POST["descripcion"];
  }
  date_default_timezone_set('America/Bogota');
  $fech_reg = date('Y-m-d H:i:s');

  $tipo = "";
  if(isset($_POST["customRadioInline1"])){
    $tipo = $_POST["customRadioInline1"];
  }

  $img_nomb = "foto_default.png";
  $src = "";
  $tiempo = new DateTime();
  $destino = "img/productos/";
  if(isset($_POST["crear"])){
    if($_FILES["foto"]["name"]  != ""){
        $auxfoto = $_FILES["foto"]["tmp_name"];
        $type_foto = $_FILES["foto"]["type"];
        $img_nomb = "img_".$nombre.$tiempo -> getTimestamp().(($type_foto == "image/png")?".png":".jpg");
        $src = $destino.$img_nomb;
        copy($auxfoto,$src);
    }else {
        $src = $destino.$img_nomb;
    }

    $producto = new Producto("",$nombre, $precio, $src, $fech_reg, $descripcion,$tipo);
    $producto -> insertar();

    date_default_timezone_set('America/Bogota');
    $fecha = date("Y-m-d");
    $hora = date("H:i:s");
    $log = new Log("","Crear_Producto","Nombre:".$nombre."&Precio:".$precio."&foto:".$src."&FechaRegistro:".$fech_reg."&Descripcion:".$descripcion."&Tipo:".$tipo,$fecha,$hora,"0",$_SESSION["id"]);
    $log -> insertar();
  }
?>
<div class="container mt-3 ">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-success text-white">
                      <h2 style="font-family: 'Fondamento', cursive; font-size:30px">REGISTRAR NUEVO PRODUCTO</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("vista/administrador/producto/regisproduc.php")?>" method="post" enctype="multipart/form-data">
                      <div class="row d-flex justify-content-center">
                          <div class="col-12 col-md-7">
                            <div class="form group">
                                <strong><label>NOMBRE</label></strong>
                                <input type="text" name="nombre" class="form-control" required>
                            </div>
                            <div class="form group">
                                <strong><label>PRECIO</label></strong>
                                <input type="number" name="precio" class="form-control" value="<?php echo $precio ?>" required>
                            </div>
                            <div class="form-group">
                              <strong><label>DESCRIPCIÓN</label></strong>
                              <textarea class="form-control" name="descripcion" rows="3"></textarea>
                            </div>
                            <strong><label>Tipo de producto</label></strong>
                            <div class="form group text-center">
                              <div class="custom-control custom-radio custom-control-inline bg-success text-white">
                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input" value="1"required>
                                <label class="custom-control-label" for="customRadioInline1">Verdura</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline bg-warning">
                                <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input" value="0" required>
                                <label class="custom-control-label" for="customRadioInline2">Fruta</label>
                              </div>
                            </div>
                            <div class="form group">
                                <strong><label>FECHA DE REGISTRO</label></strong>
                                <h5><?php echo $fech_reg?></h5>
                            </div>
                          </div>
                            <div class="dropdown-divider"></div>
                          <div class="col-12 col-md-3">
                            <div class="photo text-center" >
                                <strong><label for="foto">FOTO</label></strong>
                                      <div class="prevPhoto border border-dark">
                                      <span class="delPhoto notBlock border border-dark">X</span>
                                      <label for="foto"></label>
                                      </div>
                                      <div class="upimg border border-dark">
                                      <input type="file" name="foto" id="foto">
                                      </div>
                                      <div id="form_alert"></div>
                            </div>
                          </div>
                      </div>
                      <div class="dropdown-divider"></div>
                      <div>
                        <div class="form group">
                            <button type="submit" name="crear" class="form-control btn btn-outline-success">REGISTRAR PRODUCTO</button>
                        </div>
                        <?php if(isset($_POST["crear"])){ ?>
                        <div class="dropdown-divider"></div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <strong>Producto creado exitosamente!</strong>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                        <?php } ?>
                      </div>
                    </form>
                  </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
  $("#foto").on("change",function(){
    var uploadFoto = document.getElementById("foto").value;
      var foto       = document.getElementById("foto").files;
      var nav = window.URL || window.webkitURL;
      var contactAlert = document.getElementById('form_alert');

          if(uploadFoto !='')
          {
              var type = foto[0].type;
              var name = foto[0].name;
              if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
              {
                  contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';
                  $("#img").remove();
                  $(".delPhoto").addClass('notBlock');
                  $('#foto').val('');
                  return false;
              }else{
                      contactAlert.innerHTML='';
                      $("#img").remove();
                      $(".delPhoto").removeClass('notBlock');
                      var objeto_url = nav.createObjectURL(this.files[0]);
                      $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                      $(".upimg label").remove();

                  }
            }else{
              alert("No selecciono foto");
              $("#img").remove();
            }
  });

  $('.delPhoto').click(function(){
    $('#foto').val('');
    $(".delPhoto").addClass('notBlock');
    $("#img").remove();

  });

});
</script>
