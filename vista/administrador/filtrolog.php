<?php
$log = new Log();
$cantidad = 20;
if (isset($_GET["cantidad"])) {
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if (isset($_GET["pagina"])) {
    $pagina = $_GET["pagina"];
}
$filtro = "";
if (isset($_GET["filtro"])) {
    $filtro = $_GET["filtro"];
}
$cant = $log->consultarCantidad($filtro);
$cantPagina = intval($cant[0] / $cantidad);
if (($cant[0] % $cantidad) != 0) {
    $cantPagina++;
}
$logs = $log -> consultarFiltroPaginacion($filtro, $cantidad, $pagina);
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-12 col-lg-3">
          <div class="card">
              <div class="card-header">
                  <h3 style="font-family: 'Playfair Display', serif; font-size:30px">Filtro</h3>
              </div>
              <div class="card-body">
                <input class="form-control mr-sm-2" id="buscar" type="search" placeholder="Buscar por accion" aria-label="Search" data-cantidad="<?php echo $cantidad ?>" value="<?php echo ($filtro != null ? $filtro : "") ?>">
              </div>
          </div>
        </div>
        <div class="col-12 col-lg-9">
          <div class="card">
              <div class="card-header text-white bg-success">
                  <h4 style="font-family: 'Playfair Display', serif; font-size:30px">Registros Log</h4>
              </div>
              <div class="card-body">
                  <div id="contenido">
                      <div class="table table-responsive-sm table-responsive-md">
                          <table class="table table-responsive-sm table-responsive-md table-hover table-striped">
                            <tr class="thead-dark">
                              <th>#</th>
                              <th>Accion</th>
                              <th>Fecha</th>
                              <th>Hora</th>
                              <th>Actor</th>
                              <th>Datos</th>
                              <th></th>
                            </tr>
                            <?php
                            $i=1;
                            foreach($logs as $llogs){
                                echo "<tr>";
                                echo "<td>" . $i . "</td>";
                                echo "<td>" . $llogs -> getAccion() . "</td>";
                                echo "<td>" . $llogs -> getFecha() . "</td>";
                                echo "<td>" . $llogs -> getHora() . "</td>";
                                if($llogs -> getActor() == 0){
                                    echo "<td>Administrador</td>";
                                }elseif ($llogs -> getActor() == 1) {
                                    echo "<td>Proveedor</td>";
                                }elseif ($llogs -> getActor() == 2) {
                                    echo "<td>Cliente</td>";
                                }
                                ?>
                                      <td><button class="border border-light" type="button" data-toggle="modal" data-target="#modalcantidad<?php echo $llogs -> getId_log() ?>"> <i class="fas fa-eye"></i></button></td>
                                <?php
                                echo "</tr>";
                                $i++;
                            }
                            ?>
                          </table>
                      </div>
                      <div class="d-flex justify-content-end">
                          <nav>
                              <ul class="pagination">
                                  <?php if ($pagina > 1) {
                                      echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("vista/administrador/filtrolog.php") . '&pagina=' . ($pagina - 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &lt;&lt; </a></li>';
                                  } ?>
                                  <?php for ($i = 1; $i <= $cantPagina; $i++) {
                                      if ($pagina == $i) {
                                          echo "<li class='page-item active'>" .
                                              "<a class='page-link'>$i</a>" .
                                              "</li>";
                                      } else {
                                          echo "<li class='page-item'>" .
                                              "<a class='page-link' href='index.php?pid=" . base64_encode("vista/administrador/filtrolog.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "&filtro=" . $filtro . "'>" . $i . "</a>" .
                                              "</li>";
                                      }
                                  } ?>
                                  <?php if ($pagina < $cantPagina) {
                                      echo '<li class="page-item"> <a class="page-link" href="index.php?pid=' . base64_encode("vista/administrador/filtrolog.php") . '&pagina=' . ($pagina + 1) . '&cantidad=' . $cantidad . '&filtro=' . $filtro . '" tabindex="0" aria-disabled="false"> &gt;&gt; </a></li>';
                                  } ?>
                              </ul>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>
</div>


<?php foreach ($logs as $llogs){ ?>
  <div class="modal fade" id="modalcantidad<?php echo $llogs -> getId_log() ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            DATOS
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php
/////
////
            if($llogs -> getAccion() == "Actualizar_Cliente" ||  $llogs -> getAccion() == "Actualizar_Administrador" || $llogs -> getAccion() == "Actualizar_Proveedor"){
                $Datos = explode("&",$llogs -> getDatos());
              ?>
							<div class="row">
									<div class="col-6">
										<h4>Datos Antiguos</h4>
		                <div class="dropdown-divider"></div>
		                <p><?php
		                  for ($i=0; $i < count($Datos) ; $i++) {
		                        echo $Datos[$i]."<br>";
		                  }
		                  ?></p>
									</div>
									<div class="col-6">
										<h4>Datos Actuales</h4>
		                <div class="dropdown-divider"></div>
		              <?php
		                if($llogs -> getActor() == 0){
		                  $admin = new Administrador($llogs -> getId_actor());
		                  $admin -> consultar();
		                ?>
		                  <div>
		                    <p>Id:<?php echo $admin -> getIdAdministrador()?><br>Nombre:<?php echo $admin -> getNombre()?><br>Apellido:<?php echo $admin -> getApellido()?><br>Correo:<?php echo $admin -> getCorreo()?></p>
		                  </div>
		                <?php
		                }elseif ($llogs -> getActor() == 1) {
		                  $provee = new Proveedor($llogs -> getId_actor());
		                  $provee -> consultar();
		                ?>
		                  <p>Id:<?php echo $provee -> getIdProveedor()?><br>Nombre:<?php echo $provee -> getNombre()?><br>Telefono:<?php echo $provee -> getTelefono()?><br>Direccion:<?php echo $provee -> getDireccion()?></p>
		                <?php
		                }elseif ($llogs -> getActor() == 2) {
		                  $cli = new Cliente($llogs -> getId_actor());
		                  $cli -> consultar();
		                ?>
		                  <p>Id:<?php echo $cli -> getIdCliente()?><br>Nombre:<?php echo $cli -> getNombre()?><br>Apellido:<?php echo $cli -> getApellido()?><br>Correo:<?php echo $cli -> getCorreo()?><br>Sexo:<?php echo $cli -> getSexo() ?><br>Direccion:<?php echo $cli -> getDireccion()?></p>
		                <?php
		                }
		              ?>
									</div>
							</div>
              <div>
                  <h4>Actor</h4>
              </div>
              <?php
                if($llogs -> getActor() == 0){
                  $admin = new Administrador($llogs -> getId_actor());
                  $admin -> consultar();
                ?>
                <div class="row">
                  <div class="col-2"></div>
                   <div>
                      <img src="<?php echo $admin -> getFoto()?>" width="100px">
                   </div>
                   <div class="col-7">
                      <p>Id:<?php echo $admin -> getIdAdministrador()?><br>Nombre:<?php echo $admin -> getNombre()?><br>Correo:<?php echo $admin -> getCorreo()?>    </p>
                   </div>
                </div>
                <?php
                }elseif ($llogs -> getActor() == 1) {
                  $provee = new Proveedor($llogs -> getId_actor());
                  $provee -> consultar();
                ?>
                <div class="row">
                  <div class="col-2"></div>
                   <div>
                      <img src="<?php echo $provee -> getFoto()?>" width="100px">
                   </div>
                   <div class="col-7">
                      <p>Id:<?php echo $provee -> getIdProveedor()?><br>Nombre:<?php echo $provee -> getNombre()?><br>Correo:<?php echo $provee -> getCorreo()?></p>
                   </div>
                </div>
                <?php
                }elseif ($llogs -> getActor() == 2) {
                  $cli = new Cliente($llogs -> getId_actor());
                  $cli -> consultar();
                ?>
                <div class="row">
                  <div class="col-2"></div>
                   <div>
                      <img src="<?php echo $cli -> getFoto()?>" width="100px">
                   </div>
                   <div class="col-7">
                      <p>Id:<?php echo $cli -> getIdCliente()?><br>Nombre:<?php echo $cli -> getNombre()?><br>Correo:<?php echo $cli -> getCorreo()?></p>
                   </div>
                </div>
                <?php
                }
/////
/////
            }elseif ($llogs -> getAccion() == "Actualizar_Producto" ) {
              $Datos = explode("&",$llogs -> getDatos());
              $listaDatos = array();
              for ($i=0; $i < count($Datos) ; $i++) {
                  $DatosDatos = explode(":",$Datos[$i]);
                  for ($j=0; $j < count($DatosDatos) ; $j++) {
                      array_push($listaDatos,$DatosDatos[$j]);
                  }
              }

              ?>
							<div class="row">
									<div class="col-6">
										<h4>Datos Antiguos</h4>
										<div class="dropdown-divider"></div>
										<p><?php
											for ($i=0; $i < count($Datos) ; $i++) {
														echo $Datos[$i]."<br>";
											}
											?></p>
									</div>
									<div class="col-6">
										<h4>Datos Nuevos</h4>
										 <div class="dropdown-divider"></div>
										<?php
			              $producto = new Producto($listaDatos[1]);
			              $producto -> consultarProducto();
			              ?>
			              <p>Id:<?php echo $producto -> getIdProducto()?><br>Nombre:<?php echo $producto -> getNombre()?><br>Precio:<?php echo $producto -> getPrecio()?><br>Descripcion:<?php echo $producto -> getDescripcion()?><br>Tipo:<?php echo $producto -> getTipo()?></p>
			              <div>
									</div>
							</div>
              </div>
							<h4>Actor</h4>
                <?php
                  if($llogs -> getActor() == 0){
                    $admin = new Administrador($llogs -> getId_actor());
                    $admin -> consultar();
                  ?>
                  <div class="row">
                    <div class="col-2"></div>
                     <div>
                        <img src="<?php echo $admin -> getFoto()?>" width="100px">
                     </div>
                     <div class="col-7">
                        <p>Id:<?php echo $admin -> getIdAdministrador()?><br>Nombre:<?php echo $admin -> getNombre()?><br>Correo:<?php echo $admin -> getCorreo()?></p>
                     </div>
                  </div>
              <?php
              }
//////
//////
            }else {
              $Datos = explode("&",$llogs -> getDatos());
            ?>
							<div class="row">
								<div class="col-6">
									<h4>Datos Creados</h4>
									<div class="dropdown-divider"></div>
									<p><?php
										for ($i=0; $i < count($Datos) ; $i++) {
													echo $Datos[$i]."<br>";
										}
										?></p>
								</div>
								<div class="col-6">
									<h4>Actor</h4>
									<?php
										if($llogs -> getActor() == 0){
											$admin = new Administrador($llogs -> getId_actor());
											$admin -> consultar();
										?>
										<div class="row">
											<div class="col-2"></div>
											 <div>
													<img src="<?php echo $admin -> getFoto()?>" width="100px">
											 </div>
											 <div class="col-7">
													<p>Id:<?php echo $admin -> getIdAdministrador()?><br>Nombre:<?php echo $admin -> getNombre()?><br>Correo:<?php echo $admin -> getCorreo()?></p>
											 </div>
										</div>
										<?php
										}elseif ($llogs -> getActor() == 1) {
											$provee = new Proveedor($llogs -> getId_actor());
											$provee -> consultar();
										?>
										<div class="row">
											<div class="col-2"></div>
											 <div>
													<img src="<?php echo $provee -> getFoto()?>" width="100px">
											 </div>
											 <div class="col-7">
													<p>Id:<?php echo $provee -> getIdProveedor()?><br>Nombre:<?php echo $provee -> getNombre()?><br>Correo:<?php echo $provee -> getCorreo()?></p>
											 </div>
										</div>
										<?php
										}elseif ($llogs -> getActor() == 2) {
											$cli = new Cliente($llogs -> getId_actor());
											$cli -> consultar();
										?>
										<div class="row">
											<div class="col-2"></div>
											 <div>
													<img src="<?php echo $cli -> getFoto()?>" width="100px">
											 </div>
											 <div class="col-7">
													<p>Id:<?php echo $cli -> getIdCliente()?><br>Nombre:<?php echo $cli -> getNombre()?><br>Correo:<?php echo $cli -> getCorreo()?></p>
											 </div>
										</div>
										<?php } ?>
								</div>
							</div>
					<?php
            }
          ?>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<?php } ?>


<script>
    $(document).ready(function() {
        $("#buscar").keyup(function() {
            if ($(this).val().length >= 1 || $(this).val().length == 0) {
                var url = "indexAjax.php?pid=<?php echo base64_encode("vista/administrador/logAjax.php") ?>&filtro=" + $(this).val() + "&cantidad=" + $(this).data("cantidad");
                $("#contenido").load(url);
            }
        });
    });
</script>
