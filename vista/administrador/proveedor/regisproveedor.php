<?php
  $nombre = "";
  if(isset($_POST["nombre"])){
        $nombre = $_POST["nombre"];
  }

  $direccion = "";
  if(isset($_POST["direccion"])){
        $direccion = $_POST["direccion"];
  }

  $telefono = "";
  if(isset($_POST["telefono"])){
        $telefono = $_POST["telefono"];
  }

  $correo = "";
  if(isset($_POST["correo"])){
        $correo = $_POST["correo"]."@fruvernet.com";
  }

  $contraseña = "";
  if(isset($_POST["contraseña"])){
        $contraseña = $_POST["contraseña"];
  }

  $img_nomb = "img/sinfotocliente.png";
  $src = "";
  $band = false;
  $estado = 1;
  $tiempo = new DateTime();
  $destino = "img/imgproveedor/";
  if(isset($_POST["crear"])){
    if($_FILES["foto"]["name"]  != ""){
      $auxfoto = $_FILES["foto"]["tmp_name"];
      $type_foto = $_FILES["foto"]["type"];
      $img_nomb = "";
      $img_nomb = "img_".$nombre.$tiempo -> getTimestamp().(($type_foto == "image/png")?".png":".jpg");
      $src = $destino.$img_nomb;
      copy($auxfoto,$src);
    }else{
        $src = $img_nomb;
    }
    date_default_timezone_set('America/Bogota');
    $fecha = date("Y-m-d");
    $hora = date("H:i:s");
    $log = new Log("","Crear_Proveedor","Nombre:".$nombre."&Direccion:".$direccion."&Correo:".$correo."&Telefono:".$telefono,$fecha,$hora,"0",$_SESSION["id"]);
    $log -> insertar();

    $proveedor = new Proveedor("",$nombre, $direccion, $contraseña, $correo, $telefono, $estado,$src);
    $proveedor -> insertar();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-success text-white">
                      <h2 style="font-family: 'Fondamento', cursive; font-size:30px">REGISTRAR NUEVO PROVEEDOR</h2>
                  </div>
                  <div class="card-body">
                    <form class="" action="index.php?pid=<?php echo base64_encode("vista/administrador/proveedor/regisproveedor.php")?>" method="post" enctype="multipart/form-data">
                        <div class="row d-flex justify-content-center">
                            <div class="col-12 col-lg-7">
                              <div class="form group">
                                  <strong><label>NOMBRE</label></strong>
                                  <input type="text" name="nombre" class="form-control" required>
                              </div>
                              <div class="form group">
                                  <strong><label>DIRECCION</label></strong>
                                  <input type="text" name="direccion" class="form-control" required>
                              </div>
                              <div class="form group">
                                  <strong><label>TELEFONO</label></strong>
                                  <input type="number" name="telefono" class="form-control" required>
                              </div>
                              <div class="form group">
                                  <strong><label>CORREO</label></strong>
                                  <input type="text" name="correo" class="form-control" required>
                                  <small class="form-text text-muted">Introducir el correo sin [@...com].</small>
                              </div>
                              <div class="form group">
                                  <strong><label>CONTRASEÑA</label></strong>
                                  <input type="password" name="contraseña" class="form-control" required>
                              </div>
                            </div>
                            <div class="col-12 col-lg-3">
                              <div class="photo text-center" >
                                  <strong><label for="foto">FOTO</label></strong>
                                        <div class="prevPhoto border border-dark">
                                        <span class="delPhoto notBlock border border-dark">X</span>
                                        <label for="foto"></label>
                                        </div>
                                        <div class="upimg border border-dark">
                                        <input type="file" name="foto" id="foto">
                                        </div>
                                        <div id="form_alert"></div>
                              </div>
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <div class="form group">
                            <button type="submit" name="crear" class="form-control btn btn-outline-success">REGISTRAR PROVEEDOR</button>
                        </div>
                        <?php if(isset($_POST["crear"])){ ?>
                        <div class="dropdown-divider"></div>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          <strong>Proveedor registrado exitosamente!</strong>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                      <?php } ?>
                    </form>
                  </div>
              </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
  $("#foto").on("change",function(){
    var uploadFoto = document.getElementById("foto").value;
      var foto       = document.getElementById("foto").files;
      var nav = window.URL || window.webkitURL;
      var contactAlert = document.getElementById('form_alert');

          if(uploadFoto !='')
          {
              var type = foto[0].type;
              var name = foto[0].name;
              if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
              {
                  contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';
                  $("#img").remove();
                  $(".delPhoto").addClass('notBlock');
                  $('#foto').val('');
                  return false;
              }else{
                      contactAlert.innerHTML='';
                      $("#img").remove();
                      $(".delPhoto").removeClass('notBlock');
                      var objeto_url = nav.createObjectURL(this.files[0]);
                      $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                      $(".upimg label").remove();

                  }
            }else{
              alert("No selecciono foto");
              $("#img").remove();
            }
  });

  $('.delPhoto').click(function(){
    $('#foto').val('');
    $(".delPhoto").addClass('notBlock');
    $("#img").remove();

  });

});
</script>
