<?php
  $provee = new Proveedor();

  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayProvee = $provee -> consultarPaginacion($cantidad, $pagina);
  $totalRegistros = $provee -> consultarCantidad();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
      <div class="card">
          <div class="card-header bg-success text-white">
              <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de proveedores</h2>
          </div>
          <div class="card-body">
              <table class="table table-responsive-lg">
                  <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Direccion</th>
                        <th>Correo</th>
                        <th>Telefono</th>
                        <th>Estado</th>
                        <th>Foto</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i=1;
                        foreach ($arrayProvee as $lProvee) {
                            echo "<tr>";
                              echo "<td>".$i."</td>";
                              echo "<td>".$lProvee -> getNombre()."</td>";
                              echo "<td>".$lProvee -> getDireccion()."</td>";
                              echo "<td>".$lProvee -> getCorreo()."</td>";
                              echo "<td>".$lProvee -> getTelefono()."</td>";
                              ?>
                              <td>
                                <select class="custom-select camestado_prov" data-idprov="<?php echo $lProvee -> getIdProveedor()?>" style="width: 100px;" >
                                  <option value="1" <?php echo ($lProvee->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
                                  <option value="0" <?php echo ($lProvee->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
                                </select>
                              </td>
                              <?php
                              echo "<td><img src='" . $lProvee -> getFoto() . "' height='45px'></td>";
                            echo "</tr>";
                            $i++;
                        }
                      ?>
                  </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <nav>
                  <ul class="pagination">
                    <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/proveedor/listaproveedor.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                    <?php
                    for($i=1; $i<=$totalPaginas; $i++){
                        if($i==$pagina){
                            echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                        }else{
                            echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("vista/administrador/proveedor/listaproveedor.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                        }
                    }
                    ?>
                    <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/proveedor/listaproveedor.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                  </ul>
                </nav>
              </div>
          </div>
      </div>
</div>

<script>
  $(".camestado_prov").change(function() {
      var objJSON = {
          idProv: $(this).data("idprov"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("vista/administrador/proveedor/estadoAjaxProv.php") ?>";
      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });
</script>
