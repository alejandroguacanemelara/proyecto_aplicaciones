<?php
  $proveEA = new Proveedor($_POST["idProv"]);
  $proveEA -> consultar();

  $estadoantiguo = $proveEA -> getEstado();

  date_default_timezone_set('America/Bogota');
  $fecha = date("Y-m-d");
  $hora = date("H:i:s");
  $log = new Log("","Actualizar_Estado_Proveedor","Id:".$_POST["idProv"]."&Estado:".$estadoantiguo,$fecha,$hora,"0",$_SESSION["id"]);
  $log -> insertar();

  $proveEN = new Proveedor($_POST["idProv"],"","","","","",$_POST["estado"]);
  $proveEN -> actualizar_Estado_Prov();
?>
