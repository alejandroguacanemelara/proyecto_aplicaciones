<?php
$filtro = $_GET["filtro"];
$log = new Log();
$cantidad = 15;
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$logs = $log -> consultarFiltro($filtro,$cantidad, $pagina);
$totalRegistros = $log -> consultarCantidad($filtro);
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
$ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Registros Log</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($productos) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	<div class="card-body">
        					<table class="table table-hover table-striped">
        						<tr>
        							<th>#</th>
        							<th>Accion</th>
        							<th>Fecha</th>
        							<th>Hora</th>
                      <th>Actor</th>
                      <th>Id_actor</th>
                      <th>Datos</th>
        							<th></th>
        						</tr>
        						<?php
        						$i=1;
        						foreach($logs as $llogs){
        						    echo "<tr>";
        						    echo "<td>" . $i . "</td>";
        						    echo "<td>" . $llogs -> getAccion() . "</td>";
        						    echo "<td>" . $llogs -> getFecha() . "</td>";
        						    echo "<td>" . $llogs -> getHora() . "</td>";
                        if($llogs -> getActor() == 0){
                            echo "<td>Administrador</td>";
                        }elseif ($llogs -> getActor() == 1) {
                            echo "<td>Proveedor</td>";
                        }elseif ($llogs -> getActor() == 2) {
                            echo "<td>Cliente</td>";
                        }
                        echo "<td>".$llogs -> getId_actor()."</td>";
        						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/editarProducto.php") . "&idProducto=" . $productoActual -> getIdProducto(). "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";
        						    echo "</tr>";
        						    $i++;
        						}
        						?>
        					</table>
        					<div class="text-center">
                				<nav>
                					<ul class="pagination">
                						<li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPagina.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                						<?php
                						for($i=1; $i<=$totalPaginas; $i++){
                						    if($i==$pagina){
                						        echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                						    }else{
                						        echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPagina.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                						    }
                						}
                						?>
                						<li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("presentacion/producto/consultarProductoPagina.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                					</ul>
                				</nav>
        					</div>
				</div>
      </div>
		</div>
	</div>
</div>
