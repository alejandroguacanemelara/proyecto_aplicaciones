<?php
  $img_nomb = "sinfotoadmin.jpg";
  $destino = "img/imgadmin/imgPerfil/";
  $src = "";
  $tiempo = new DateTime();
  if(isset($_POST["crear"])){
    if($_FILES["foto"]["name"]  != ""){
      $auxfoto = $_FILES["foto"]["tmp_name"];
      $type_foto = $_FILES["foto"]["type"];
      $img_nomb = "img_".$_POST["nombre"].$tiempo -> getTimestamp().(($type_foto == "image/png")?".png":".jpg");
      $src = $destino.$img_nomb;
      copy($auxfoto,$src);
      $admin = new Administrador($_SESSION["id"]);
      $admin -> consultar();
      if($admin -> getFoto() != "img/sinfotoadmin.jpg"){
          unlink($admin -> getFoto());
      }

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Administrador","Id:".$_SESSION["id"]."&Nombre:".$admin -> getNombre()."&Apellido:".$admin -> getApellido()."&Correo:".$admin -> getCorreo(),$fecha,$hora,"0",$_SESSION["id"]);
      $log -> insertar();

      $admin = new Administrador($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],$_POST["correo"],$src);
      $admin -> actualizar();
    }else {
      $admin = new Administrador($_SESSION["id"]);
      $admin -> consultar();
      if($admin -> getFoto() != ""){
          $img_nomb = $admin -> getFoto();
      }

      date_default_timezone_set('America/Bogota');
      $fecha = date("Y-m-d");
      $hora = date("H:i:s");
      $log = new Log("","Actualizar_Administrador","Id:".$_SESSION["id"]."&Nombre:".$admin -> getNombre()."&Apellido:".$admin -> getApellido()."&Correo:".$admin -> getCorreo(),$fecha,$hora,"0",$_SESSION["id"]);
      $log -> insertar();

      $src = $img_nomb;
      $admin = new Administrador($_SESSION["id"],$_POST["nombre"],$_POST["apellido"],$_POST["contraseña"],$_POST["correo"],$src);
      $admin -> actualizar();
    }
  }else {
     $admin = new Administrador($_SESSION["id"]);
     $admin -> consultar();
  }
?>
<div class="container mt-3">
    <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-8">
              <div class="card">
                  <div class="card-header text-center bg-success text-white">
                      <h2 style="font-family: 'Fondamento', cursive; font-size:30px">EDITAR ADMINISTRADOR</h2>
                  </div>
                  <div class="card-body">
                      <div class="row">
                          <div class="col-12 col-md-7">
                            <form class="" action="index.php?pid=<?php echo base64_encode("vista/administrador/infoadmin/editarinfoadmin.php")?>" method="post" enctype="multipart/form-data">
                                <div class="form group">
                                    <strong><label>NOMBRE</label></strong>
                                    <input type="text" name="nombre" class="form-control" value="<?php echo $admin -> getNombre() ?>" required>
                                </div>
                                <div class="form group">
                                    <strong><label>APELLIDO</label></strong>
                                    <input type="text" name="apellido" class="form-control" value="<?php echo $admin -> getApellido() ?>" required>
                                </div>
                                <div class="form group">
                                    <strong><label>CORREO</label></strong>
                                    <input type="text" name="correo" class="form-control" value="<?php echo $admin -> getCorreo() ?>" required>
                                </div>
                                <div class="form group">
                                    <strong><label>CONTRASEÑA</label></strong>
                                    <input type="password" name="contraseña" class="form-control" required>
                                </div>
                          </div>
                          <div class="col-12 col-md-5">
                            <div class="photo text-center" >
                                <strong><label for="foto">FOTO</label></strong>
                                      <div class="prevPhoto border border-dark">
                                      <span class="delPhoto notBlock border border-dark">X</span>
                                      <label for="foto"></label>
                                      </div>
                                      <div class="upimg border border-dark">
                                      <input type="file" name="foto" id="foto">
                                      </div>
                                      <div id="form_alert"></div>
                              </div>
                            <div class="dropdown-divider"></div>
                          </div>
                      </div>
                      <div class="dropdown-divider"></div>
                      <div class="form group">
                          <button type="submit" name="crear" class="form-control btn btn-outline-success">ACTUALIZAR ADMINISTRADOR</button>
                      </div>
                      <?php if(isset($_POST["crear"])){ ?>
                      <div class="dropdown-divider"></div>
                      <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Actualizacion exitosamente!</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                            </button>
                      </div>
                    <?php } ?>
                  </div>
              </div>
        </div>
    </div>

</div>


<script>
$(document).ready(function(){
  $("#foto").on("change",function(){
    var uploadFoto = document.getElementById("foto").value;
      var foto       = document.getElementById("foto").files;
      var nav = window.URL || window.webkitURL;
      var contactAlert = document.getElementById('form_alert');

          if(uploadFoto !='')
          {
              var type = foto[0].type;
              var name = foto[0].name;
              if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
              {
                  contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';
                  $("#img").remove();
                  $(".delPhoto").addClass('notBlock');
                  $('#foto').val('');
                  return false;
              }else{
                      contactAlert.innerHTML='';
                      $("#img").remove();
                      $(".delPhoto").removeClass('notBlock');
                      var objeto_url = nav.createObjectURL(this.files[0]);
                      $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                      $(".upimg label").remove();

                  }
            }else{
              alert("No selecciono foto");
              $("#img").remove();
            }
  });

  $('.delPhoto').click(function(){
    $('#foto').val('');
    $(".delPhoto").addClass('notBlock');
    $("#img").remove();

  });

});
</script>
