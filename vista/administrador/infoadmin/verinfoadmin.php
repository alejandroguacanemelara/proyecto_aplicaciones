<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container mt-3">
	<div class="row d-flex justify-content-center">
		<div class="col-12 col-lg-8 ">
      <div class="card">
					<div class="card-header text-white bg-success">
							<h4 style="font-family: 'Playfair Display', serif; font-size:30px">INFORMACION DEL ADMINISTRADOR</h4>
					</div>
          <div class="card-body">
            <div class="row d-flex justify-content-center">
              <div class="col-12 col-md-5">
              		<img src="<?php echo ($administrador -> getFoto() != "")?$administrador -> getFoto():"img/sinfotoadmin.jpg"; ?>" width="100%" class="rounded-left">
            	</div>
              <div class="col-12 col-md-7 d-flex align-items-center">
								<table class="table table-hover">
										<tr>
											<th>Nombre</th>
											<td><?php echo $administrador -> getNombre() ?></td>
										</tr>
										<tr>
											<th>Apellido</th>
											<td><?php echo $administrador -> getApellido() ?></td>
										</tr>
										<tr>
											<th>Correo</th>
											<td><?php echo $administrador -> getCorreo() ?></td>
										</tr>
									</table>
								</div>
              </div>
            </div>
          </div>
		</div>
	</div>
</div>
