<?php
  $cli = new Cliente();
  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayCli = $cli -> consultarPaginacion($cantidad, $pagina);
  $totalRegistros = $cli -> consultarCantidad();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);;
?>
<div class="container mt-3">
      <div class="card">
          <div class="card-header bg-success text-white">
              <h2 style="font-family: 'Playfair Display', serif; font-size:30px">Lista de clientes</h2>
          </div>
          <div class="card-body">
              <table class="table table-responsive-lg">
                  <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Correo</th>
                        <th>Sexo</th>
                        <th>Estado</th>
                        <th>Foto</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i=1;
                        foreach ($arrayCli as $lCli) {
                            echo "<tr>";
                              echo "<td>".$i."</td>";
                              echo "<td>".$lCli -> getNombre()."</td>";
                              if($lCli -> getApellido() != ""){
                                echo "<td>".$lCli -> getApellido()."</td>";
                              }else {
                                echo "<td>-</td>";
                              }
                              echo "<td>".$lCli -> getCorreo()."</td>";
                              if($lCli -> getSexo() != ""){
                                if($lCli -> getSexo() == 0){
                                    echo "<td>Masculino</td>";
                                }elseif ($lCli -> getSexo() == 1){
                                    echo "<td>Femenino</td>";
                                }else {
                                    echo "<td>No definido</td>";
                                }
                              }else {
                                echo "<td>-</td>";
                              }
                              ?>
                              <td>
                                <select class="custom-select camestado_cli" data-idcli="<?php echo $lCli -> getIdCliente()?>" style="width: 100px;" >
                                  <option value="1" <?php echo ($lCli->getEstado() == 1) ? 'selected' : '' ?>>Activo</option>
                                  <option value="0" <?php echo ($lCli->getEstado() == 0) ? 'selected' : '' ?>>Inactivo</option>
                                  <option value="-1" <?php echo ($lCli->getEstado() == -1) ? 'selected' : '' ?>>Bloqueado</option>
                                </select>
                              </td>

                              <?php
                              echo "<td><img src='" . $lCli -> getFoto() . "' height='45px'></td>";
                            echo "</tr>";
                            $i++;
                        }
                      ?>
                  </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <nav>
                  <ul class="pagination">
                    <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/cliente/listacliente.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                    <?php
                    for($i=1; $i<=$totalPaginas; $i++){
                        if($i==$pagina){
                            echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                        }else{
                            echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("vista/administrador/cliente/listacliente.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                        }
                    }
                    ?>
                    <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/cliente/listacliente.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                  </ul>
                </nav>
              </div>
          </div>
      </div>
</div>

<script>
  $(".camestado_cli").change(function() {
      var objJSON = {
          idCli: $(this).data("idcli"),
          estado: $(this).val(),
      }

      url = "indexAjax.php?pid=<?php echo base64_encode("vista/administrador/cliente/estadoAjaxCli.php") ?>";

      $.post(url, objJSON, function(info) {
          $res = JSON.parse(info);
      })

  });
</script>
