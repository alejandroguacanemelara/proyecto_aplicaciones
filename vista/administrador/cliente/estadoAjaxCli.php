<?php
  $cliEA = new Cliente($_POST["idCli"]);
  $cliEA -> consultar();

  $estadoantiguo = $cliEA -> getEstado();

  date_default_timezone_set('America/Bogota');
  $fecha = date("Y-m-d");
  $hora = date("H:i:s");
  $log = new Log("","Actualizar_Estado_Cliente","Id:".$_POST["idCli"]."&Estado:".$estadoantiguo,$fecha,$hora,"0",$_SESSION["id"]);
  $log -> insertar();

  $cliEN = new Cliente($_POST["idCli"],"","","","","","",$_POST["estado"]);
  $cliEN -> actualizar_Estado_Cli();

?>
