<?php
  $venta = new Venta();
  $cantidad = 5;
  if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
  }
  $pagina = 1;
  if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
  }
  $arrayVen = $venta -> consultarPaginacion($cantidad, $pagina);
  $totalRegistros = $venta -> consultarCantidad();
  $totalPaginas = intval($totalRegistros/$cantidad);
  if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
  }
  $ultimaPagina = ($totalPaginas == $pagina);
?>
<div class="container mt-3">
  <div class="row d-flex justify-content-center">
    <div class="col-12 col-lg-8">
      <div class="card">
          <div class="card-header bg-success text-white">
              <h2 style="font-family: 'Playfair Display', serif; font-size:35px">Ventas</h2>
          </div>
          <div class="card-body">
              <table class="table table-responsive-lg">
                  <thead class="thead-dark">
                      <tr>
                        <th>#</th>
                        <th>Identicacion</th>
                        <th>Precio</th>
                        <th>Fecha_ven</th>
                        <th>Hora_ven</th>
                        <th>-</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i=1;
                        foreach ($arrayVen as $lVen) {
                            echo "<tr>";
                              echo "<td>".$i."</td>";
                              echo "<td>".$lVen -> getId_venta()."</td>";
                              echo "<td>$ ".$lVen -> getPrecio_ven()."</td>";
                              echo "<td>".$lVen -> getFecha_ven()."</td>";
                              echo "<td>".$lVen -> getHora_ven()."</td>";
                        ?>
                              <td><button class="border border-light" type="button" data-toggle="modal" data-target="#modalventa<?php echo $lVen -> getId_venta() ?>"> <i class="fas fa-eye"></i></button></td>
                        <?php
                            echo "</tr>";
                            $i++;
                        }
                      ?>
                  </tbody>
            </table>
            <div class="d-flex justify-content-end">
                <nav>
                  <ul class="pagination">
                    <li class="page-item <?php echo ($pagina==1)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/listaventas.php") . "&pagina=" . ($pagina-1) . "&cantidad=" . $cantidad ?>"> &lt;&lt; </a></li>
                    <?php
                    for($i=1; $i<=$totalPaginas; $i++){
                        if($i==$pagina){
                            echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                        }else{
                            echo "<li class='page-item'><a class='page-link' href='index.php?pid=" . base64_encode("vista/administrador/listaventas.php") . "&pagina=" . $i . "&cantidad=" . $cantidad . "'>" . $i . "</a></li>";
                        }
                    }
                    ?>
                    <li class="page-item <?php echo ($ultimaPagina)?"disabled": ""; ?>"><a class="page-link" href="<?php echo "index.php?pid=" . base64_encode("vista/administrador/listaventas.php") . "&pagina=" . ($pagina+1) . "&cantidad=" . $cantidad ?>"> &gt;&gt; </a></li>
                  </ul>
                </nav>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>

<?php foreach ($arrayVen as $lVen){ ?>
  <div class="modal fade" id="modalventa<?php echo $lVen -> getId_venta() ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h3 style="font-family: 'Playfair Display', serif; font-size:30px" > <?php echo $lVen -> getId_venta()." - Id_Cliente ".$lVen -> getId_clientefk() ?> </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row d-flex justify-content-center">
            <?php $cliente = new Cliente($lVen -> getId_clientefk());
                  $cliente -> consultar();
            ?>
              <div class="col-4">
                  <img src="<?php echo $cliente -> getFoto() ?>" width="200px">
              </div>
              <div class="col-3">
                  <p><strong>Nombre:</strong> <br> <?php echo $cliente -> getNombre() ?></p>
                  <br>
                  <p>
                    <strong>Apellido: </strong> <br> <?php echo $cliente -> getApellido() ?>
                  </p>
                  <p>
                    <strong>Correo: </strong> <br> <?php echo $cliente -> getCorreo() ?>
                  </p>
              </div>
          </div>
          <div class="dropdown-divider"></div>
          <div class="d-flex flex-sm-wrap">
            <?php
                $prod_ven = new Prod_ven($lVen -> getId_venta());
                $arrayprod_ven = $prod_ven -> consultarVentas_v();

                foreach ($arrayprod_ven as $lprodven) {
                  $producto = new Producto($lprodven -> getId_productofk());
                  $producto -> consultarProducto();
                  ?>
                  <div class="card col-3" style="width: 8rem;">
                    <img src="<?php echo $producto -> getFoto() ?>" class="card-img-top"  width="50px">
                    <div class="card-body">
                      <h5 class="card-title"><?php echo $producto -> getNombre() ?> - <?php echo $lprodven -> getCantidad() ?> Lb.</h5>
                    </div>
                  </div>
            <?php
                }
             ?>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<?php } ?>
