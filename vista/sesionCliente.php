<div class="container mt-3 ">
  <div>
      <div id="carruselinicio" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="img/1.jpg" class="d-block h-75 w-100" >
            </div>
            <div class="carousel-item">
              <img src="img/2.jpg" class="d-block h-75 w-100" >
            </div>
            <div class="carousel-item">
              <img src="img/3.jpg" class="d-block h-75 w-100" >
            </div>
            <div class="carousel-item">
              <img src="img/4.jpg" class="d-block h-75 w-100" >
            </div>
          </div>
          <a class="carousel-control-prev" href="#carruselinicio" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Anterior</span>
          </a>
          <a class="carousel-control-next" href="#carruselinicio" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Siguiente</span>
          </a>
    </div>
  </div>
</div>
<div class="container mt-3 " style="background-color: #000000; opacity: 0.8;">
  <div class="row text-white d-flex justify-content-center">
    <div class="col-12 d-flex justify-content-around  col-md-2 d-flex flex-column bd-highlight mb-3">
      <div>
        <img src="img/calidadinicio.png" width="100px" height="100px" class="rounded-circle">
      </div>
    </div>
    <div class="col-12 d-flex justify-content-around col-md-3">
      <div class="mt-3" style="font-family: 'Merienda One', cursive; font-size:13px">
        <h3>Calidad</h3>
        La empresa Fruver.NET cuenta con la mejora calidad en frutas y vegetales, contamos con los mejores empresas, campesinos y agricultores que cultiven el mejor producto.
      </div>
    </div>
    <div class="col-12 d-flex justify-content-around col-md-2 d-flex flex-column bd-highlight mb-3">
      <div>
        <img src="img/envios.png" width="100px" height="100px" class="rounded-circle">
      </div>
    </div>
    <div class="col-12 d-flex justify-content-around col-md-3">
      <div class="mt-3" style="font-family: 'Merienda One', cursive; font-size:13px">
        <h3>Envios</h3>
        La empresa Fruver.NET cuenta con los envios mas rapidos a nivel nacional, permitiendo la satisfaccion del cliente y que tenga el mejor trato posible.
      </div>
  </div>
 </div>
</div>
<div class="container mt-2" style="background-color: #000000;">
  <div class="row text-white d-flex justify-content-center">
    <div class="col-4">
      <div class="mt-4"></div>
      <div>
        <h3 class="text-center" style="font-family: 'Merienda One', cursive; font-size:15px" >Todos los derechos reservados <i class="fa fa-registered" aria-hidden="true"></i></h3>
      </div>
    </div>
  </div>
</div>
