<?php
  session_start();
  require_once "modelo/Administrador.php";
  require_once "modelo/Proveedor.php";
  require_once "modelo/Cliente.php";
  require_once "modelo/Producto.php";
  require_once "modelo/Venta.php";
  require_once "modelo/Proveedor_producto.php";
  require_once "modelo/Prod_ven.php";
  require_once "modelo/Log.php";

  $pid = "";
  if(isset($_GET["pid"])){
      $pid = base64_decode($_GET["pid"]);
  }else{
      $_SESSION["id"]="";
      $_SESSION["rol"]="";
      if($_SESSION["rol"]=="Cliente"){
        $_SESSION["carrito"] = array();
        $_SESSION["cantidad"] = array();
      }
  }
  if(isset($_GET["salir"]) || !isset($_SESSION["id"])){
      $_SESSION["id"]="";
      $_SESSION["carrito"] = array();
      $_SESSION["cantidad"] = array();
  }

?>
  <head>
    <link rel="icon" type="image/png" href="img/productoicon.png" />
  	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link rel="stylesheet" href="estilos.css">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa&family=Merienda+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Fondamento:ital@1&family=Playfair+Display:ital@1&display=swap" rel="stylesheet">
  	<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
  	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
    <meta charset="utf-8">
    <title>Fruver.NET</title>
  </head>
  <body  background="img/fondo9.jpg">
      <?php
              $pagwoutsession = array(
        	  	"vista/autenticacion.php",
              "vista/cliente/regiscliente.php",
              "vista/cliente/activarcuenta.php"
        		  );
              if(in_array($pid, $pagwoutsession)){
  	               include $pid;
  	          }else if($_SESSION["id"]!="") {
  	               if($_SESSION["rol"] == "Administrador"){
  	                     include "vista/menuAdmin.php";
  	          }else if($_SESSION["rol"] == "Proveedor"){
  	               include "vista/menuProveedor.php";
  	          }elseif ($_SESSION["rol"] == "Cliente") {
                  include "vista/menuCliente.php";
              }
  	             include $pid;
  	          }else{
  	             include "vista/encabezado.php";
                 include "vista/inicio.php";
        		}

       ?>
  </body>
</html>
