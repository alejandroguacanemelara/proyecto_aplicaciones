<?php
  class ProductoDAO{

    private $idProducto;
    private $nombre;
    private $precio;
    private $foto;
    private $fech_reg;
    private $descripcion;
    private $tipo;
    private $inventario;

        public function ProductoDAO($idProducto="",$nombre="",$precio="",$foto="",$fech_reg="",$descripcion="",$tipo="", $inventario=""){
              $this -> idProducto = $idProducto;
              $this -> nombre = $nombre;
              $this -> precio = $precio;
              $this -> foto = $foto;
              $this -> fech_reg = $fech_reg;
              $this -> descripcion = $descripcion;
              $this -> tipo = $tipo;
              $this -> inventario = $inventario;
        }

        public function insertar(){
            return "insert into producto (nombre, precio, fech_reg, descripcion,tipo".(($this -> foto!="")?", foto":"").")
                    values ('" . $this -> nombre . "', '" . $this -> precio . "', '" . $this -> fech_reg . "', '". $this -> descripcion ."', '".$this -> tipo."'". (($this -> foto!="")?", '" . $this -> foto . "')":")");
        }

        public function consultarCantidad(){
            return "select count(id_producto)
                    from producto";
        }

        public function consultarProducto(){
            return "select nombre, precio, descripcion, foto, inventario, tipo
                    from producto
                    where id_producto = '" . $this -> idProducto .  "'";
        }
        public function consultarProductos(){
          return "select id_producto, nombre, precio, fech_reg, descripcion, foto,tipo, inventario
                  from producto";
        }

        public function actualizar(){
            return "update producto
                    set nombre = '" . $this -> nombre . "', precio = '" . $this -> precio . "', descripcion = '".$this -> descripcion ."', tipo = '".$this -> tipo."'".(($this -> foto!="")?", foto = '" . $this -> foto . "'":"") . "
                    where id_producto = '" . $this -> idProducto .  "'";
        }

        public function actualizarInventario(){
            return "update producto
                    set inventario = '".$this -> inventario."'
                    where id_producto = '" . $this -> idProducto .  "'";
        }

        public function consultarPaginacion($cantidad, $pagina){
                return "select id_producto, nombre, precio, fech_reg, descripcion, foto,tipo, inventario
                        from producto
                        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }


        public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
                return "select id_producto, nombre, precio, fech_reg, descripcion, foto,tipo, inventario
                        from producto
                        where nombre like '%" . $filtro . "%' order by precio limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
       }

       public function consultarCantidad_filtro($filtro){
        return "select count(id_producto)
                from producto
                where nombre like '%".$filtro."%'";
        }
  }

?>
