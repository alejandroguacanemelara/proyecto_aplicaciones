<?php
  class ProveedorDAO{

    private $idProveedor;
    private $nombre;
    private $direccion;
    private $contraseña;
    private $correo;
    private $telefono;
    private $estado;
    private $foto;

        public function ProveedorDAO($idProveedor="",$nombre="",$direccion="",$contraseña="",$correo="",$telefono="",$estado="",$foto=""){
              $this -> idProveedor = $idProveedor;
              $this -> nombre = $nombre;
              $this -> direccion = $direccion;
              $this -> contraseña = $contraseña;
              $this -> correo = $correo;
              $this -> telefono = $telefono;
              $this -> estado = $estado;
              $this -> foto = $foto;
        }

        public function insertar(){
            return "insert into proveedor (nombre, direccion, telefono, correo, contraseña, estado_prov,foto)
                    values ('". $this -> nombre . "', '" . $this -> direccion . "', '" . $this -> telefono . "', '" . $this -> correo. "', '" . md5($this -> contraseña) . "','".$this -> estado."', '".$this -> foto."')";
        }

        public function validar(){
          return "select id_proveedor, nombre, estado_prov
                  from proveedor
                  where correo='".$this -> correo."' and contraseña = '".md5($this -> contraseña)."'";
        }

        public function consultar(){
            return "select nombre,direccion,correo,telefono,foto,estado_prov
                    from proveedor
                    where id_proveedor= '".$this -> idProveedor."'";
        }

        public function consultarCantidad(){
            return "select count(id_proveedor)
                    from proveedor";
        }

        public function consultarProveedores(){
          return "select id_proveedor, nombre, direccion, correo, telefono, estado_prov,foto
                  from proveedor";
        }

        public function consultarPaginacion($cantidad, $pagina){
                return "select id_proveedor, nombre, direccion, correo, telefono, estado_prov,foto
                        from proveedor
                        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function actualizar(){
            return "update proveedor
                    set nombre = '" . $this -> nombre . "', direccion = '" . $this -> direccion . "', contraseña = '".md5($this -> contraseña)."', foto = '".$this -> foto ."', telefono ='".$this -> telefono."'
                    where id_proveedor = '" . $this -> idProveedor .  "'";
        }

        public function actualizar_Estado_Prov(){
            return "update proveedor
                    set estado_prov = '" . $this -> estado . "'
                    where id_proveedor = '" . $this -> idProveedor .  "'";
        }
    }
?>
