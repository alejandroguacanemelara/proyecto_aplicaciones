<?php
  class ClienteDAO{

    private $idCliente;
    private $nombre;
    private $apellido;
    private $contraseña;
    private $correo;
    private $foto;
    private $sexo;
    private $estado;
    private $direccion;

        public function ClienteDAO($idCliente="",$nombre="",$apellido="",$contraseña="",$correo="",$foto="",$sexo="", $estado="",$direccion){
              $this -> idCliente = $idCliente;
              $this -> nombre = $nombre;
              $this -> apellido = $apellido;
              $this -> contraseña = $contraseña;
              $this -> correo = $correo;
              $this -> foto = $foto;
              $this -> sexo = $sexo;
              $this -> estado = $estado;
              $this -> direccion = $direccion;
        }

        public function actualizar(){
            return "update cliente
                    set nombre = '" . $this -> nombre . "', apellido = '" . $this -> apellido . "', contraseña = '".md5($this -> contraseña)."', foto = '".$this -> foto ."', sexo ='".$this -> sexo."', direccion ='".$this -> direccion."'
                    where id_cliente = '" . $this -> idCliente .  "'";
        }

        public function consultarCantidad(){
            return "select count(id_cliente)
                    from cliente";
        }

        public function validar(){
            return "select id_cliente,nombre,apellido,estado
                    from cliente
                    where correo = '" . $this -> correo .  "' and contraseña = '" . md5($this -> contraseña) . "'";
        }

        public function existeCorreo(){
            return "select correo
                    from cliente
                    where correo = '" . $this -> correo .  "'";
        }

        public function insertar($codigoActivacion){
            return "insert into cliente (nombre, correo, contraseña, estado, codigo_activ,foto)
                    values ('".$this -> nombre."','" . $this -> correo . "', '" . md5($this -> contraseña) . "', '-1', '" . md5($codigoActivacion) . "' , '".$this -> foto."')";
        }

        public function activar(){
            return "update cliente
                    set estado = '1'
                    where correo = '" . $this -> correo .  "'";
        }

        public function verificarCodigoActivacion($codigoActivacion){
            return "select id_cliente
                    from cliente
                    where correo = '" . $this -> correo .  "' and codigo_activ = '" . md5($codigoActivacion) . "'";
        }

        public function consultarClientes(){
          return "select id_cliente, nombre, apellido, correo,sexo, estado,foto
                  from cliente";
        }

        public function consultar(){
          return "select nombre, apellido, correo, foto, sexo, direccion, estado
                  from cliente
                  where id_cliente='".$this -> idCliente."'";
        }

        public function consultarid(){
            return "select id_cliente
                    from cliente
                    where correo = '" . $this -> correo .  "'";
        }

        public function actualizar_Estado_Cli(){
            return "update cliente
                    set estado = '" . $this -> estado. "'
                    where id_cliente = '" . $this -> idCliente .  "'";
        }

        public function consultarPaginacion($cantidad, $pagina){
                return "select id_cliente, nombre, apellido, correo,sexo, estado,foto
                        from cliente
                        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }
  }
?>
