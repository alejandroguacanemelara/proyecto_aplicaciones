-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 15-07-2020 a las 06:06:32
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_aplicaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `nombre`, `apellido`, `contraseña`, `correo`, `foto`) VALUES
(456789, 'Valeria', 'Montes', '202cb962ac59075b964b07152d234b70', 'valeriam45@fruvernet.com', 'img/sinfotoadmin.jpg'),
(987985, 'Maria', 'Rodriguez Castro', '202cb962ac59075b964b07152d234b70', 'mariarc456@fruvernet.com', 'img/imgadmin/imgPerfil/img_Maria1594607883.jpg'),
(1000780789, 'Pablo Alejandro', 'Guacaneme Lara', 'e10adc3949ba59abbe56e057f20f883e', 'alejandrog@fruvernet.com', 'img/imgadmin/imgPerfil/img_Pablo Alejandro1593929576.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` bigint(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) DEFAULT NULL,
  `contraseña` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `sexo` int(3) DEFAULT NULL,
  `estado` tinyint(11) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `codigo_activ` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `apellido`, `contraseña`, `correo`, `foto`, `sexo`, `estado`, `direccion`, `codigo_activ`) VALUES
(1, 'Homer', 'Simpson', '202cb962ac59075b964b07152d234b70', 'homer@asd.com', 'img/sinfotocliente.png', 0, 1, 'Av654#54norte', NULL),
(2, 'Maria Jose', 'Valdez Perez', '202cb962ac59075b964b07152d234b70', 'alejo@gmail.com', 'img/imgcliente/img_Maria1594341338.jpg', 1, 1, 'Cra87#87D83sur', 'dfd7468ac613286cdbb40872c8ef3b06'),
(3, 'alejo', NULL, '202cb962ac59075b964b07152d234b70', 'alejooo1@gmail.com', 'img/sinfotocliente.png', NULL, 1, NULL, '624ec1c881656ee6418604df2928494b'),
(4, 'Jose Luis', NULL, '202cb962ac59075b964b07152d234b70', 'Jose564@gmail.com', 'img/sinfotocliente.png', NULL, 0, NULL, '1387a00f03b4b423e63127b08c261bdc'),
(6, 'Felipe', NULL, '202cb962ac59075b964b07152d234b70', 'feli485@gmail.com', 'img/sinfotocliente.png', NULL, -1, NULL, '08f0efebb1c51aada9430a089a2050cc'),
(7, 'Paola', 'Diaz', '202cb962ac59075b964b07152d234b70', 'paola456@hotmail.com', 'img/imgcliente/img_Paola1594768252.jpg', 0, 1, 'Cra87#87D83sur', 'acc21473c4525b922286130ffbfe00b5'),
(8, 'Diana', NULL, '202cb962ac59075b964b07152d234b70', 'diana213@gmail.com', 'img/sinfotocliente.png', NULL, -1, NULL, 'f056bfa71038e04a2400266027c169f9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Log`
--

CREATE TABLE `Log` (
  `id_log` int(11) NOT NULL,
  `accion` varchar(50) NOT NULL,
  `datos` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `actor` int(11) NOT NULL,
  `id_actor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `Log`
--

INSERT INTO `Log` (`id_log`, `accion`, `datos`, `fecha`, `hora`, `actor`, `id_actor`) VALUES
(165, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '20:20:03', 0, 1000780789),
(166, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '20:21:27', 0, 1000780789),
(167, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '20:21:43', 0, 1000780789),
(168, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '20:21:47', 2, 2),
(169, 'Crear_venta', '445815947761353830', '2020-07-14', '20:22:15', 2, 2),
(170, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:22:42', 1, 1),
(171, 'Agregar_Cantidad', 'Id_Producto:12&Id_Proveedor:1&Cantidad:50', '2020-07-14', '20:23:07', 1, 1),
(172, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:1', '2020-07-14', '20:23:34', 0, 1000780789),
(173, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:0', '2020-07-14', '20:23:42', 0, 1000780789),
(174, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:-1', '2020-07-14', '20:23:53', 0, 1000780789),
(175, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:24:07', 1, 1),
(176, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '20:24:36', 2, 2),
(177, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:1', '2020-07-14', '20:25:10', 0, 1000780789),
(178, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:-1', '2020-07-14', '20:39:06', 0, 1000780789),
(179, 'Actualizar_Estado_Proveedor', 'Id:1&Estado:0', '2020-07-14', '20:39:20', 0, 1000780789),
(180, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:39:29', 1, 1),
(181, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:40:13', 1, 1),
(182, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:40:49', 1, 1),
(183, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '20:41:36', 0, 1000780789),
(184, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '20:41:45', 1, 1),
(185, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '20:42:46', 0, 1000780789),
(186, 'InicioSesionProveedor', 'joseemilio1597@fruvernet.com&Jose Emilio', '2020-07-14', '20:43:44', 1, 3),
(187, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '20:45:47', 0, 1000780789),
(188, 'Crear_Producto', 'Nombre:Mango&Precio:3500&foto:img/productos/foto_default.png&FechaRegistro:2020-07-14 20:48:57&Descripcion:Delicioso mango&Tipo:0', '2020-07-14', '20:48:57', 0, 1000780789),
(189, 'Actualizar_Producto', 'Id:19&Nombre:Mango&Precio:3500&Descripcion:Delicioso mango&Tipo:0', '2020-07-14', '20:49:40', 0, 1000780789),
(190, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '20:50:17', 0, 1000780789),
(191, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '20:50:29', 0, 1000780789),
(192, 'Actualizar_Estado_Cliente', 'Id:2&Estado:-1', '2020-07-14', '20:50:38', 0, 1000780789),
(193, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '20:50:43', 2, 2),
(194, 'Actualizar_Producto', 'Id:19&Nombre:Mango&Precio:3500&Descripcion:Delicioso mango&Tipo:0', '2020-07-14', '20:58:23', 0, 1000780789),
(195, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '21:01:22', 0, 1000780789),
(196, 'Crear_Administrador', 'Id:456789&Nombre:Valeria&Apellido:Montes&Correo:valeriam45@fruvernet.com', '2020-07-14', '21:03:43', 0, 1000780789),
(197, 'InicioSesionProveedor', 'distribuidoraA@fruvernet.com&DistribuidoraA', '2020-07-14', '21:06:04', 1, 1),
(198, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '21:08:47', 0, 1000780789),
(199, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '21:11:34', 0, 1000780789),
(200, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:1', '2020-07-14', '21:11:49', 0, 1000780789),
(201, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:0', '2020-07-14', '21:12:01', 0, 1000780789),
(202, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:-1', '2020-07-14', '21:12:14', 0, 1000780789),
(203, 'InicioSesionProveedor', 'joseemilio1597@fruvernet.com&Jose Emilio', '2020-07-14', '21:12:19', 1, 3),
(204, 'Agregar_Cantidad', 'Id_Producto:19&Id_Proveedor:3&Cantidad:45', '2020-07-14', '21:13:03', 1, 3),
(205, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '21:13:39', 2, 2),
(206, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '21:14:06', 0, 1000780789),
(207, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '21:14:19', 0, 1000780789),
(208, 'Actualizar_Estado_Cliente', 'Id:2&Estado:-1', '2020-07-14', '21:14:31', 0, 1000780789),
(209, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '21:14:35', 2, 2),
(210, 'Crear_venta', '334715947793889167', '2020-07-14', '21:16:28', 2, 2),
(211, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '21:19:13', 0, 1000780789),
(212, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:1', '2020-07-14', '21:22:26', 0, 1000780789),
(213, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:0', '2020-07-14', '21:22:34', 0, 1000780789),
(214, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:-1', '2020-07-14', '21:22:45', 0, 1000780789),
(215, 'InicioSesionProveedor', 'joseemilio1597@fruvernet.com&Jose Emilio', '2020-07-14', '21:22:50', 1, 3),
(216, 'Agregar_Cantidad', 'Id_Producto:3&Id_Proveedor:3&Cantidad:20', '2020-07-14', '21:24:05', 1, 3),
(217, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '21:24:42', 0, 1000780789),
(218, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '21:24:53', 0, 1000780789),
(219, 'Actualizar_Estado_Cliente', 'Id:2&Estado:-1', '2020-07-14', '21:25:02', 0, 1000780789),
(220, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '21:25:08', 2, 2),
(221, 'Crear_venta', '567615947799752691', '2020-07-14', '21:26:15', 2, 2),
(222, 'Crear_venta', '769115947813638777', '2020-07-14', '21:49:23', 2, 2),
(223, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '21:54:28', 2, 2),
(224, 'Crear_venta', '899515947816833829', '2020-07-14', '21:54:43', 2, 2),
(225, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '22:01:01', 0, 1000780789),
(226, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:1', '2020-07-14', '22:01:11', 0, 1000780789),
(227, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '22:03:01', 0, 1000780789),
(228, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:0', '2020-07-14', '22:06:24', 0, 1000780789),
(229, 'InicioSesionProveedor', 'joseemilio1597@fruvernet.com&Jose Emilio', '2020-07-14', '22:06:29', 1, 3),
(230, 'Agregar_Cantidad', 'Id_Producto:2&Id_Proveedor:3&Cantidad:15', '2020-07-14', '22:06:46', 1, 3),
(231, 'Actualizar_Cantidad', 'Id_Producto:2&Id_Proveedor:3&Cantidad:5', '2020-07-14', '22:07:17', 1, 3),
(232, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '22:07:54', 0, 1000780789),
(233, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '22:08:05', 0, 1000780789),
(234, 'Actualizar_Estado_Cliente', 'Id:2&Estado:-1', '2020-07-14', '22:08:16', 0, 1000780789),
(235, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '22:08:21', 2, 2),
(236, 'Crear_venta', '404415947825531626', '2020-07-14', '22:09:13', 2, 2),
(237, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '22:13:35', 0, 1000780789),
(238, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '22:17:34', 0, 1000780789),
(239, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:1', '2020-07-14', '22:19:50', 0, 1000780789),
(240, 'Actualizar_Estado_Proveedor', 'Id:3&Estado:0', '2020-07-14', '22:19:59', 0, 1000780789),
(241, 'InicioSesionProveedor', 'joseemilio1597@fruvernet.com&Jose Emilio', '2020-07-14', '22:20:03', 1, 3),
(242, 'Agregar_Cantidad', 'Id_Producto:7&Id_Proveedor:3&Cantidad:10', '2020-07-14', '22:20:36', 1, 3),
(243, 'Agregar_Cantidad', 'Id_Producto:13&Id_Proveedor:3&Cantidad:10', '2020-07-14', '22:20:52', 1, 3),
(244, 'Actualizar_Estado_Cliente', 'Id:2&Estado:1', '2020-07-14', '22:21:33', 0, 1000780789),
(245, 'Actualizar_Estado_Cliente', 'Id:2&Estado:0', '2020-07-14', '22:21:41', 0, 1000780789),
(246, 'Actualizar_Estado_Cliente', 'Id:2&Estado:-1', '2020-07-14', '22:21:50', 0, 1000780789),
(247, 'InicioSesionCliente', 'alejo@gmail.com&Maria Jose&Valdez Perez', '2020-07-14', '22:21:55', 2, 2),
(248, 'Crear_venta', '229915947833755999', '2020-07-14', '22:22:55', 2, 2),
(249, 'Crear_Producto', 'Nombre:Piña&Precio:3500&foto:img/productos/img_Piña1594783472.jpg&FechaRegistro:2020-07-14 22:24:32&Descripcion:Jugosa&Tipo:0', '2020-07-14', '22:24:32', 0, 1000780789),
(250, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '22:26:17', 0, 1000780789),
(251, 'InicioSesionAdministrador', 'alejandrog@fruvernet.com&Pablo Alejandro&Guacaneme Lara', '2020-07-14', '23:01:00', 0, 1000780789);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` bigint(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `precio` bigint(20) NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `fech_reg` datetime NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `Tipo` int(1) NOT NULL,
  `inventario` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre`, `precio`, `foto`, `fech_reg`, `descripcion`, `Tipo`, `inventario`) VALUES
(2, 'Manzana', 3400, 'img/productos/img_Manzana1593908824.jpg', '2020-04-07 19:07:04', 'La mejor manzana de Colombia.  Rica y nutritiva', 0, 55),
(3, 'Mandarina', 2750, 'img/productos/img_Mandarina1593909077.jpg', '2020-07-04 19:07:17', 'La mejor fruta para refrescarse y con vitamina C, que nos ayuda a tener mas energia.', 0, 37),
(6, 'Aguacate', 4550, 'img/productos/img_Aguacate1594305496.jpg', '2020-07-04 19:07:00', 'El aguacate mas bueno del país. Delicioso.', 1, 20),
(7, 'Brocoli', 4350, 'img/productos/img_Brocoli1594305624.jpg', '2020-07-04 20:07:59', 'sowqeojeqwojewqjodojsadjo', 1, 47),
(8, 'Rabano', 3500, 'img/productos/img_Rabano1594305675.jpg', '2020-07-04 20:07:32', 'wqeewqoijwsdjoipdsaoijasdiojdsajio', 1, 25),
(9, 'Fresa', 5000, 'img/productos/img_Fresa1594305718.jpg', '2020-07-04 21:07:00', 'Peodoiqjoiqdjoiwdqji', 0, 38),
(10, 'Naranja', 3500, 'img/productos/img_Naranja1594305818.jpg', '2020-07-04 21:07:56', 'Dulcecita josadjkldsajkladsjlkkajdsl', 0, 43),
(11, 'Cebolla cabezona', 2550, 'img/productos/img_Cebolla cabezona1594307451.jpg', '2020-07-04 21:07:57', 'wqjwqeopiweqoiuweqoip', 1, 43),
(12, 'Repollo', 5300, 'img/productos/img_Repollo1594307553.jpg', '2020-07-04 21:07:32', 'oiqwiopqweiopweqoip', 1, 50),
(13, 'Melon', 3800, 'img/productos/img_Melon1594307617.jpg', '2020-07-04 21:07:46', 'qwiojwqeoiweqiouwqeiou', 0, 67),
(14, 'Sandia', 5800, 'img/productos/img_Sandia1594307803.jpg', '2020-07-09 10:16:43', 'La sandia mas deliciosa de colombia', 0, 17),
(15, 'Durazno', 3250, 'img/productos/img_Durazno1594494869.jpg', '2020-07-11 14:14:29', 'qeqweqwasdere', 0, 34),
(16, 'Cebolla Larga', 7100, 'img/productos/foto_default.png', '2020-07-11 17:18:23', 'qpoweiqpoieqipoqepowi', 1, 32),
(17, 'Pera', 3100, 'img/productos/foto_default.png', '2020-07-11 17:23:16', 'qweqeqeqe', 0, 50),
(18, 'Uva', 2500, 'img/productos/img_Uva1594507570.jpg', '2020-07-11 17:46:10', 'qeeqwewqdsaewrqdfsrewdaeraw', 0, 44),
(19, 'Mango', 3500, 'img/productos/img_Mango1594778303.jpg', '2020-07-14 20:48:57', 'Delicioso mango', 0, 41),
(20, 'Piña', 3500, 'img/productos/img_Piña1594783472.jpg', '2020-07-14 22:24:32', 'Jugosa', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prod_ven`
--

CREATE TABLE `prod_ven` (
  `id_producto_fk` bigint(20) NOT NULL,
  `id_venta_fk` bigint(20) NOT NULL,
  `cantidad` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `prod_ven`
--

INSERT INTO `prod_ven` (`id_producto_fk`, `id_venta_fk`, `cantidad`) VALUES
(2, 256015943952304997, 5),
(2, 422515943594453614, 8),
(2, 445815947761353830, 5),
(2, 496615945733568164, 5),
(2, 695815946009404758, 1),
(2, 758515946798598884, 1),
(2, 826015944064325838, 5),
(3, 174215946487357239, 3),
(3, 256015943952304997, 1),
(3, 404415947825531626, 2),
(3, 422515943594453614, 5),
(3, 496615945733568164, 8),
(3, 550515943591783068, 3),
(3, 695815946009404758, 1),
(3, 701615947362154597, 1),
(3, 847115947625052299, 4),
(3, 983715945193538057, 2),
(6, 422515943594453614, 12),
(6, 657315945965065596, 5),
(6, 695815946009404758, 2),
(6, 747115946046162566, 5),
(6, 758515946798598884, 3),
(6, 983715945193538057, 3),
(7, 422515943594453614, 5),
(7, 747115946046162566, 5),
(7, 983715945193538057, 3),
(8, 550515943591783068, 2),
(8, 695815946009404758, 3),
(9, 559015946484631259, 4),
(10, 229915947833755999, 2),
(10, 559015946484631259, 5),
(11, 657315945965065596, 4),
(11, 747115946046162566, 7),
(13, 559015946484631259, 3),
(14, 334715947793889167, 3),
(14, 657315945965065596, 1),
(14, 747115946046162566, 4),
(15, 404415947825531626, 3),
(18, 174215946487357239, 10),
(18, 229915947833755999, 5),
(18, 567615947799752691, 5),
(18, 899515947816833829, 1),
(19, 334715947793889167, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` bigint(20) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `telefono` bigint(20) NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `estado_prov` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre`, `contraseña`, `correo`, `direccion`, `telefono`, `foto`, `estado_prov`) VALUES
(1, 'DistribuidoraA', '202cb962ac59075b964b07152d234b70', 'distribuidoraA@fruvernet.com', 'Cll84#65norte', 3216549878, 'img/sinfotocliente.png', 1),
(2, 'DistribuidoraB', '202cb962ac59075b964b07152d234b70', 'distribuidoraB@fruvernet.com', 'Av654#54norte', 1472583690, 'img/sinfotocliente.png', 1),
(3, 'Jose Emilio', '202cb962ac59075b964b07152d234b70', 'joseemilio1597@fruvernet.com', 'Av654#54norte', 3789456123, 'img/imgproveedor/img_Jose Emilio1594772649.jpg', 1),
(4, 'Maria Victoria', '202cb962ac59075b964b07152d234b70', 'mariavictoria1578@fruvernet.com', 'Cll84#65norteasd', 7984562130, 'img/sinfotocliente.png', 1),
(7, 'David Perez', '202cb962ac59075b964b07152d234b70', 'daviperez4561@fruvernet.com', 'Cra87#87D33sur', 3216549870, 'img/imgproveedor/img_David Perez1594569545.png', 1),
(8, 'Rosa Jimenez', '202cb962ac59075b964b07152d234b70', 'rosajimenez3215@fruvernet.com', 'Cra87#87D83sur', 3216548972, 'img/sinfotocliente.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor_producto`
--

CREATE TABLE `proveedor_producto` (
  `id_proveedor_fk` bigint(20) NOT NULL,
  `id_producto_fk` bigint(20) NOT NULL,
  `cantidad` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor_producto`
--

INSERT INTO `proveedor_producto` (`id_proveedor_fk`, `id_producto_fk`, `cantidad`) VALUES
(1, 2, 55),
(1, 3, 30),
(1, 6, 50),
(1, 11, 55),
(1, 12, 50),
(1, 14, 25),
(2, 2, 10),
(2, 3, 5),
(2, 7, 50),
(2, 8, 30),
(3, 2, 20),
(3, 3, 20),
(3, 7, 10),
(3, 13, 10),
(3, 15, 40),
(3, 16, 32),
(3, 18, 65),
(3, 19, 45),
(4, 9, 42),
(4, 13, 60),
(7, 3, 12),
(7, 10, 50),
(7, 17, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` bigint(50) NOT NULL,
  `id_cliente_fk` bigint(20) NOT NULL,
  `precio_ven` bigint(20) NOT NULL,
  `fecha_ven` date NOT NULL,
  `hora_ven` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `id_cliente_fk`, `precio_ven`, `fecha_ven`, `hora_ven`) VALUES
(174215946487357239, 1, 33250, '2020-07-13', '06:42:22'),
(229915947833755999, 2, 19500, '2020-07-14', '22:22:55'),
(256015943952304997, 2, 19550, '2020-07-10', '14:26:36'),
(334715947793889167, 2, 31400, '2020-07-14', '09:42:47'),
(404415947825531626, 2, 15250, '2020-07-14', '22:09:13'),
(422515943594453614, 2, 122550, '2020-07-10', '12:24:19'),
(445815947761353830, 2, 17000, '2020-07-14', '10:23:29'),
(496615945733568164, 7, 39750, '2020-07-12', '13:22:14'),
(550515943591783068, 2, 15400, '2020-07-10', '18:05:23'),
(559015946484631259, 4, 48900, '2020-07-13', '11:31:33'),
(567615947799752691, 2, 12500, '2020-07-14', '14:52:43'),
(657315945965065596, 2, 38750, '2020-07-12', '03:20:50'),
(695815946009404758, 2, 25900, '2020-07-12', '00:06:58'),
(701615947362154597, 7, 2750, '2020-07-14', '05:28:04'),
(747115946046162566, 7, 85550, '2020-07-12', '08:38:11'),
(758515946798598884, 2, 17050, '2020-07-13', '05:26:19'),
(826015944064325838, 2, 16750, '2020-07-10', '06:33:25'),
(847115947625052299, 2, 11000, '2020-07-14', '17:44:32'),
(899515947816833829, 2, 2500, '2020-07-14', '21:54:43'),
(983715945193538057, 2, 32200, '2020-07-11', '21:21:38');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `Log`
--
ALTER TABLE `Log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `prod_ven`
--
ALTER TABLE `prod_ven`
  ADD PRIMARY KEY (`id_producto_fk`,`id_venta_fk`),
  ADD KEY `id_venta_fk` (`id_venta_fk`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `proveedor_producto`
--
ALTER TABLE `proveedor_producto`
  ADD PRIMARY KEY (`id_proveedor_fk`,`id_producto_fk`),
  ADD KEY `id_producto_fk` (`id_producto_fk`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_cliente_fk` (`id_cliente_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `Log`
--
ALTER TABLE `Log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `prod_ven`
--
ALTER TABLE `prod_ven`
  ADD CONSTRAINT `prod_ven_ibfk_1` FOREIGN KEY (`id_producto_fk`) REFERENCES `producto` (`id_producto`),
  ADD CONSTRAINT `prod_ven_ibfk_2` FOREIGN KEY (`id_venta_fk`) REFERENCES `venta` (`id_venta`);

--
-- Filtros para la tabla `proveedor_producto`
--
ALTER TABLE `proveedor_producto`
  ADD CONSTRAINT `proveedor_producto_ibfk_1` FOREIGN KEY (`id_proveedor_fk`) REFERENCES `proveedor` (`id_proveedor`),
  ADD CONSTRAINT `proveedor_producto_ibfk_2` FOREIGN KEY (`id_producto_fk`) REFERENCES `producto` (`id_producto`);

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`id_cliente_fk`) REFERENCES `cliente` (`id_cliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
