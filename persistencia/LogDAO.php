<?php

  class LogDAO{

    private $id_log;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $actor;
    private $id_actor;



        public function LogDAO($id_log="",$accion="",$datos="",$fecha="",$hora="",$actor="",$id_actor=""){
              $this -> id_log = $id_log;
              $this -> accion = $accion;
              $this -> datos = $datos;
              $this -> fecha = $fecha;
              $this -> hora = $hora;
              $this -> actor = $actor;
              $this -> id_actor = $id_actor;
        }

        public function insertar(){
           return "insert into Log(accion, datos, fecha, hora, actor, id_actor)
                   values ('".$this -> accion."','".$this -> datos."','".$this -> fecha."','".$this -> hora."','".$this -> actor."','".$this -> id_actor."')";
        }

        public function consultarFiltro($filtro){
                return "select id_log, accion, datos, fecha, hora, actor, id_actor
                        from Log
                        where accion like '%" . $filtro . "%' order by actor ";
        }

        public function consultarFiltroPaginacion($filtro,$cantidad, $pagina){
                return "select id_log, accion, datos, fecha, hora, actor, id_actor
                        from Log
                        where accion like '%" . $filtro . "%' order by id_log desc limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
       }

       public function consultarCantidad($filtro){
        return "select count(id_log)
                from Log
                where accion like '%".$filtro."%'";
        }

  }

?>
