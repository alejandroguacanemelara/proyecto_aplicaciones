<?php
  class VentaDAO{

    private $id_venta;
    private $id_clientefk;
    private $precio_ven;
    private $fecha_ven;
    private $hora_ven;

        public function VentaDAO($id_venta="",$id_clientefk="",$precio_ven="",$fecha_ven="",$hora_ven=""){
              $this -> id_venta = $id_venta;
              $this -> id_clientefk = $id_clientefk;
              $this -> precio_ven = $precio_ven;
              $this -> fecha_ven = $fecha_ven;
              $this -> hora_ven = $hora_ven;
        }

        public function insertar(){
            return "insert into venta (id_venta,id_cliente_fk,precio_ven,fecha_ven,hora_ven)
                    values ('".$this -> id_venta."' , '".$this -> id_clientefk."', '".$this -> precio_ven."', '".$this -> fecha_ven."', '".$this -> hora_ven."')";
        }

        public function consultarVentas(){
            return "select id_venta,id_cliente_fk,precio_ven,fecha_ven,hora_ven
                    from venta order by fecha_ven, hora_ven desc";
        }

        public function consultarVentasCli(){
            return "select id_venta,id_cliente_fk,precio_ven,fecha_ven,hora_ven
                    from venta
                    where id_cliente_fk = '".$this -> id_clientefk."' order by fecha_ven, hora_ven desc";
        }

        public function consultarVentasCliPaginacion($cantidad, $pagina){
            return "select id_venta,id_cliente_fk,precio_ven,fecha_ven,hora_ven
                    from venta
                    where id_cliente_fk = '".$this -> id_clientefk."' order by fecha_ven desc, hora_ven desc
                    limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }

        public function consultarCantidadVenCli(){
            return "select count(id_venta)
                    from venta
                    where id_cliente_fk = '".$this -> id_clientefk."'";
        }

        public function consultarCantidad(){
            return "select count(id_venta)
                    from venta";
        }

        public function consultarPaginacion($cantidad, $pagina){
                return "select id_venta,id_cliente_fk,precio_ven,fecha_ven,hora_ven
                        from venta order by fecha_ven desc, hora_ven desc
                        limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
        }
  }
?>
